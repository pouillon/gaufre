GAUFRE
======

GAUFRE stands for GAUssian Fitting of Radial Entities. It replaces an input
radial function by a sum of Gaussians. Although it is mainly used to fit
natural atomic orbitals, GAUFRE can process any kind of function defined
between zero and infinity inclusively and decaying to zero beyond a certain
radius.

To be continued ...

