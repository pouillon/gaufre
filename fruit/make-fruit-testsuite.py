#!/usr/bin/env python3

import copy
import os
import re
import sys

import yaml
try:
    from yaml import CLoader as MyLoader
except ImportError:
    from yaml import Loader as MyLoader


                    # ------------------------------------ #
                    # ------------------------------------ #


# Template process to run a unit test and register its result
fruit_unittest_template = """
    ! Test {index:04d}: {name}{setup}
    write(*, fmt='(/2X,"..",A)') &
&     "running test: {name}"
    call set_unit_name("{name}")
    call run_test_case({name}, &
&     "{name}")
    if ( is_case_passed() ) then
      call case_passed_xml("{name}", &
&       "{module}")
    else
      call case_failed_xml("{name}", &
&       "{module}")
    end if{teardown}
"""

# Template for a FRUIT basket, that is a wrapper module running all unit tests
# contained in a specified module. The routines must start with 'test_',
# except setup() and teardown()
fruit_basket_template = """\
!% Copyright (C) 2018-2019 Yann Pouillon <devops@materialsevolution.es>
!%
!% This Source Code Form is subject to the terms of the Mozilla Public License,
!% version 2.0. If a copy of the MPL was not distributed with this file, You
!% can obtain one at https://mozilla.org/MPL/2.0/.
!%
!% This Source Code Form is distributed in the hope that it will be useful,
!% but WITHOUT ANY WARRANTY; without even the implied warranty of
!% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!% Mozilla Public License version 2.0 for more details.

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

module fruit_basket_{module}

  use fruit

  implicit none

contains

  subroutine fruit_basket()

    {use_test_module}

    implicit none

    {tests}

  end subroutine fruit_basket

end module fruit_basket_{module}
"""

# Template for test programs
fruit_program_template = """\
!% Copyright (C) 2018-2019 Yann Pouillon <devops@materialsevolution.es>
!%
!% This Source Code Form is subject to the terms of the Mozilla Public License,
!% version 2.0. If a copy of the MPL was not distributed with this file, You
!% can obtain one at https://mozilla.org/MPL/2.0/.
!%
!% This Source Code Form is distributed in the hope that it will be useful,
!% but WITHOUT ANY WARRANTY; without even the implied warranty of
!% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!% Mozilla Public License version 2.0 for more details.

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

program fruit_test_{module}

  use fruit
  use fruit_basket_{module}

  implicit none

  logical :: all_tests_ok

  call init_fruit()
  call init_fruit_xml()

  call fruit_basket()
  call is_all_successful(all_tests_ok)

  call fruit_summary()
  call fruit_summary_xml()
  call fruit_finalize()

  if ( .not. all_tests_ok ) then
    stop 1
  end if

end program fruit_test_{module}
"""

# Makefile target template compatible with FRUIT
fruit_target_template = """\
# Test program for {module}
fruit_test_{module}_SOURCES = {test_source} \\
  fruit_basket_{module}.F90 \\
  fruit_test_{module}.F90
fruit_test_{module}_LDADD = \\
  {fruit_home}/libfruit.la \\
  {libraries}
fruit_test_{module}_DEPENDENCIES = \\
  {fruit_home}/libfruit.la \\
  {libraries}
"""

# Explicit dependencies for compatibility with make -j
fruit_dep_template = """\
# Explicit dependencies for fruit_test_{module}
fruit_test_{module}.$(OBJEXT): fruit_basket_{module}.$(OBJEXT) {test_object}
fruit_basket_{module}.$(OBJEXT): {test_object}
"""

# FRUIT makefile template optimized for Automake
fruit_makefile_template = """\
## Copyright (C) 2018-2019 Yann Pouillon <devops@materialsevolution.es>
##
## This Source Code Form is subject to the terms of the Mozilla Public License,
## version 2.0. If a copy of the MPL was not distributed with this file, You
## can obtain one at https://mozilla.org/MPL/2.0/.
##
## This Source Code Form is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
## Mozilla Public License version 2.0 for more details.

#
# FRUIT-based unit tests
#

# Build parameters
AM_FCFLAGS = {includes}

# Remove temporary test outputs
MOSTLYCLEANFILES = result_tmp.xml test_*.tmp

# Remove Fortran modules and test reports
CLEANFILES = *.$(MODEXT) result.xml

# Basic test programs
fruit_unit_tests ={test_programs}

# Test programs expected to fail
fruit_xfail_tests ={xfail_programs}

# GAUFRE test suite
TESTS = $(fruit_unit_tests) $(fruit_xfail_tests)
XFAIL_TESTS = $(fruit_xfail_tests)

# Data files for the unit tests
EXTRA_DIST = \
  gaufre_orbital_test_01.siesta \
  gaufre_orbital_test_02.siesta

# Test programs to build
check_PROGRAMS = $(fruit_unit_tests) $(fruit_xfail_tests)

{targets}

{dependencies}

# Data files to remove before 'make dist'
DISTCLEANFILES = \
  gaufre_orbital_test_01.siesta.orb \
  gaufre_orbital_test_02.siesta.orb
"""


                    # ------------------------------------ #
                    # ------------------------------------ #


class FruitTestCase(object):


    def __init__(self, fruit_specs):

        pass


                    # ------------------------------------ #
                    # ------------------------------------ #


class FruitTestSuite(object):


    def __init__(self, yml_config=".fruit.yml"):

        # Import configuration
        self.config = {}
        if ( os.path.exists(yml_config) ):
            with open(yml_config, "r") as cfg_file:
                self.config = yaml.load(cfg_file, Loader=MyLoader)

        # Set core parameters
        self.modules = self.config["project"]["modules"]
        self.destdir = self.config["project"]["tests"]
        self.libraries = []
        for srcdir, lbl in self.config["project"]["libraries"]:
            self.libraries.append(os.path.join(
                os.path.relpath(srcdir, start=self.destdir), "lib"+lbl+".la"))


    def gen_fruit_basket(self, mod_name):

        # Extract information from the module source file
        have_setup = False
        have_teardown = False
        test_cases = []
        mod_srcfile = os.path.join(self.destdir, mod_name+"_test.F90")
        if ( os.path.exists(mod_srcfile) ):
            re_routine = re.compile("^[ ]*subroutine[ ]*([^\\(\\n]*)",
                         flags=re.MULTILINE|re.IGNORECASE)
            re_tcase = re.compile("^test_", flags=re.IGNORECASE)
            re_setup = re.compile("^setup", flags=re.IGNORECASE)
            re_tdown = re.compile("^teardown", flags=re.IGNORECASE)
            with open(mod_srcfile, "r") as mod_file:
                for routine in re_routine.finditer(mod_file.read()):
                    tcase = routine.group(1)
                    if ( re_setup.search(tcase) ):
                        have_setup = True
                    elif ( re_tdown.search(tcase) ):
                        have_teardown = True
                    elif ( re_tcase.search(tcase) ):
                        test_cases.append(tcase)

        # Build the list of tests
        tcase_list = ""
        base_specs = {
            "module": mod_name,
            "setup": "",
            "teardown": "",
        }
        mod_specs = {
            "module": mod_name,
            "tests": "stop 1",
            "use_test_module": "",
        }
        if ( len(test_cases) > 0 ):
            if ( have_setup ):
                base_specs["setup"] = "\n    call setup()"
            if ( have_teardown ):
                base_specs["teardown"] = "\n    call teardown()"
            tnum = 1
            for tcase in test_cases:
                tcase_specs = copy.deepcopy(base_specs)
                tcase_specs["name"] = tcase
                tcase_specs["index"] = tnum
                tcase_list += fruit_unittest_template.format(**tcase_specs)
                tnum += 1
            mod_specs["tests"] = tcase_list.strip()
            mod_specs["use_test_module"] = "use " + mod_name + "_test"

        return fruit_basket_template.format(**mod_specs)


    def gen_fruit_test(self, mod_name):

        return fruit_program_template.format(module=mod_name)


    def gen_gitignore(self):

        ign_patterns = "*.log\n*.mod\n*.o\n*.tmpi\n"
        for mod_name in self.modules:
            for root in ["fruit_basket_", "fruit_test_"]:
                ign_patterns += root + mod_name + ".F90\n"
            ign_patterns += "fruit_test_" + mod_name + "\n"

        # FIXME: hard-coded for now
        ign_patterns += "gaufre_orbital_test_*.siesta.orb\n"

        return ign_patterns


    def gen_makefile(self):

        inc_dirs = [os.path.relpath(item, start=self.destdir) for item in self.config["project"]["sources"]] + \
            [os.path.relpath(self.config["fruit"]["home"], start=self.destdir)]
        xfail_specs = " \\\n  ".join(["fruit_test_"+item for item in self.modules
            if not os.path.exists(os.path.join(self.destdir, item+"_test.F90"))])
        if ( len(xfail_specs) > 0 ):
            xfail_specs = " \\\n  " + xfail_specs

        mf_specs = {
            "includes": "-I" + " -I".join(inc_dirs),
            "test_programs": " \\\n  " + \
                " \\\n  ".join(["fruit_test_"+item for item in self.modules
                    if os.path.exists(os.path.join(self.destdir, item+"_test.F90"))]),
            "xfail_programs": xfail_specs,
            "targets": "",
            "dependencies": "",
        }

        for mod_name in self.modules:
            mf_specs["targets"] += "\n" + self.gen_mf_target(mod_name)
            mf_specs["dependencies"] += "\n" + self.gen_mf_deps(mod_name)
        mf_specs["targets"] = mf_specs["targets"].strip()
        mf_specs["dependencies"] = mf_specs["dependencies"].strip()

        return fruit_makefile_template.format(**mf_specs)


    def gen_mf_deps(self, mod_name):

        mod_srcfile = os.path.join(self.destdir, mod_name+"_test.F90")
        mod_objfile = ""
        if ( os.path.exists(mod_srcfile) ):
            mod_objfile = mod_name + "_test.$(OBJEXT)"

        return fruit_dep_template.format(module=mod_name,
                   test_object=mod_objfile)


    def gen_mf_target(self, mod_name):

        tgt_specs = {
            "fruit_home": os.path.relpath(self.config["fruit"]["home"],
                              start=self.destdir),
            "libraries": " \\\n  ".join(self.libraries),
            "module": mod_name,
            "test_source": "",
        }
        mod_srcfile = os.path.join(self.destdir, mod_name+"_test.F90")
        if ( os.path.exists(mod_srcfile) ):
            tgt_specs["test_source"] = " \\\n  " + mod_name + "_test.F90"

        return fruit_target_template.format(**tgt_specs)


    def save(self):

        ign_path = os.path.join(self.destdir, ".gitignore")
        with open(ign_path, "w") as ign_file:
            ign_file.write(self.gen_gitignore())

        mf_path = os.path.join(self.destdir, "Makefile.am")
        with open(mf_path, "w") as mf_file:
            mf_file.write(self.gen_makefile())

        for mod_name in self.modules:
            fb_path = os.path.join(self.destdir, "fruit_basket_{}.F90".format(mod_name))
            with open(fb_path, "w") as fb_file:
                fb_file.write(self.gen_fruit_basket(mod_name))
            ft_path = os.path.join(self.destdir, "fruit_test_{}.F90".format(mod_name))
            with open(ft_path, "w") as ft_file:
                ft_file.write(self.gen_fruit_test(mod_name))


                    # ------------------------------------ #
                    # ------------------------------------ #


my_suite = FruitTestSuite()
my_suite.save()
