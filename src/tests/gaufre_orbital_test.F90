!! Copyright (C) 2018 Yann Pouillon <devops@materialsevolution.es>
!!
!! This file is part of GAUFRE.
!!
!! This Source Code Form is subject to the terms of the Mozilla Public License,
!! version 2.0. If a copy of the MPL was not distributed with this file, You
!! can obtain one at https://mozilla.org/MPL/2.0/.
!!
!! GAUFRE is distributed in the hope that it will be useful, but WITHOUT ANY
!! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
!! FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
!! more details.

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

module gaufre_orbital_test

  use fruit
  use gaufre_common
  use gaufre_orbital

  implicit none

  public

  integer, parameter, private :: RAW_NPTS = 100
  real(gfdp), parameter, private :: tol = sqrt(epsilon(1.0_gfdp))

  integer, private :: i
  real(gfdp), target, private :: xraw(RAW_NPTS) = [ &
&   (real(i-1, gfdp)/20.0_gfdp, i=1,RAW_NPTS)]
  real(gfdp), target, private :: yraw(RAW_NPTS) = [ &
&   ( ((real(i-1, gfdp)/20.0_gfdp-5.0_gfdp)**2) * &
&     ((real(i-1, gfdp)/20.0_gfdp+5.0_gfdp)**2) / 1000.0_gfdp, i=1,RAW_NPTS)]

contains

  ! **************************************************************************
  ! *** Constructor and destructor                                         ***
  ! **************************************************************************

  !> \brief Checks that gaufre_orbital_t classes are actually invalid before
  !!        initialization
  subroutine test_gaufre_orbital_invalid_before_init()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - class is invalid before init")

  end subroutine test_gaufre_orbital_invalid_before_init

  !> \brief Checks that gaufre_orbital_t%init still produces invalid
  !! configurations
  !!
  !! The init subroutine comes from gaufre_data_t, which is not aware
  !! of the physical parameters needed to describe an orbital, hence
  !! it will leave the orbital-specific fields unset.
  subroutine test_gaufre_orbital_invalid_after_init()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)

    call assert_true(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - underlying gaufre_data_t is valid after init")
    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - class is invalid after init")

    call gorb%free()

  end subroutine test_gaufre_orbital_invalid_after_init

  !> \brief Checks that gaufre_orbital_t%free produces invalid configurations
  subroutine test_gaufre_orbital_invalid_after_free()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)
    call gorb%free()

    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - class is invalid after free")
    call assert_false(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - underlying gaufre_data_t is invalid after free")

  end subroutine test_gaufre_orbital_invalid_after_free

  ! **************************************************************************
  ! *** Auxilliary constructor: init_physics                               ***
  ! **************************************************************************

  !> \brief Checks that gaufre_orbital_t%init_physics detects invalid
  !! principal quantum numbers
  subroutine test_gaufre_orbital_init_physics_detects_qn_n()

    implicit none

    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)
    call gorb%init_physics("Xyz", 0, 2, 1, .false., 2.0_gfdp, gfstat=errno)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_orbital_t - invalid qn_n produces an error")
    call assert_equals(-1, gorb%qn_n, &
&     "gaufre_orbital_t - invalid qn_n is ignored")

    call gorb%free()

  end subroutine test_gaufre_orbital_init_physics_detects_qn_n

  !> \brief Checks that gaufre_orbital_t%init_physics detects invalid
  !! angular momenta
  subroutine test_gaufre_orbital_init_physics_detects_qn_l()

    implicit none

    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)

    call gorb%init_physics("Xyz", 3, -2, 1, .true., 0.5_gfdp)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_orbital_t - negative qn_l produces an error")
    call assert_equals(-1, gorb%qn_l, &
&     "gaufre_orbital_t - negative qn_l is ignored")

    call gorb%init_physics("Xyz", 2, 2, 1, .true., 0.5_gfdp)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_orbital_t - non-physical qn_l produces an error")
    call assert_equals(-1, gorb%qn_n, &
&     "gaufre_orbital_t - non-physical qn_l is ignored")

    call gorb%free()

  end subroutine test_gaufre_orbital_init_physics_detects_qn_l

  !> \brief Checks that gaufre_orbital_t%init_physics detects invalid
  !! basis set zetas
  subroutine test_gaufre_orbital_init_physics_detects_zeta()

    implicit none

    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)
    call gorb%init_physics("Xyz", 3, 2, 0, .false., 2.0_gfdp, gfstat=errno)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_orbital_t - invalid zeta produces an error")
    call assert_equals(-1, gorb%zeta, &
&     "gaufre_orbital_t - invalid zeta is ignored")

    call gorb%free()

  end subroutine test_gaufre_orbital_init_physics_detects_zeta

  !> \brief Checks that gaufre_orbital_t%init_physics detects invalid
  !! orbital populations
  subroutine test_gaufre_orbital_init_physics_detects_population()

    implicit none

    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)
    call gorb%init_physics("Xyz", 3, 2, 1, .false., -2.0_gfdp, gfstat=errno)

    call assert_true(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - underlying gaufre_data_t is valid")
    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_orbital_t - invalid population produces an error")
    call assert_equals(-1.0_gfdp, gorb%population, &
&     "gaufre_orbital_t - invalid population is ignored")

    call gorb%free()

  end subroutine test_gaufre_orbital_init_physics_detects_population

  !> \brief Checks that physical parameters are actually transmitted to
  !! the class
  subroutine test_gaufre_orbital_init_physics_sets_params()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)
    call gorb%init_physics("Xyz", 3, 2, 1, .true., 0.5_gfdp)

    call assert_true(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - underlying gaufre_data_t is valid")
    call assert_equals("Xyz", gorb%species, &
&     "gaufre_orbital_t - init_physics sets orbital species symbol")
    call assert_equals(3, gorb%qn_n, &
&     "gaufre_orbital_t - init_physics sets orbital n quantum number")
    call assert_equals(2, gorb%qn_l, &
&     "gaufre_orbital_t - init_physics sets orbital l quantum number")
    call assert_equals(1, gorb%zeta, &
&     "gaufre_orbital_t - init_physics sets orbital zeta")
    call assert_true(gorb%polarized, &
&     "gaufre_orbital_t - init_physics sets orbital polarization flag")
    call assert_equals(0.5_gfdp, gorb%population, &
&     "gaufre_orbital_t - init_physics sets orbital population")

    call gorb%free()

  end subroutine test_gaufre_orbital_init_physics_sets_params

  ! **************************************************************************
  ! *** Alternative constructor: from_siesta                               ***
  ! **************************************************************************

  !> \brief Checks that gaufre_orbital_t%from_siesta properly loads
  !! normal orbitals
  subroutine test_gaufre_orbital_from_siesta_loads_normal_orb

    implicit none

    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%from_siesta("gaufre_orbital_test_01.siesta.orb", gfstat=errno)

    call assert_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_orbital_t - from_siesta can read orbital file")
    call assert_true(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - underlying gaufre_data_t is valid")
    call assert_true(gorb%is_valid(), &
&     "gaufre_orbital_t - orbital data is valid")

    call assert_equals(500, gorb%get_npts(), &
&     "gaufre_orbital_t - number of data points is 500")
    call assert_equals(5.99544167012_gfdp, gorb%get_rcut(), tol, &
&     "gaufre_orbital_t - cutoff radius is 5.99544167012")

    call assert_equals("Ga ", gorb%species, &
&     "gaufre_orbital_t - species symbol is left-adjusted 'Ga '")
    call assert_equals(3, gorb%qn_n, &
&     "gaufre_orbital_t - principal quantum number is 3")
    call assert_equals(2, gorb%qn_l, &
&     "gaufre_orbital_t - angular momentum is 2 (d orbital)")
    call assert_equals(1, gorb%zeta, &
&     "gaufre_orbital_t - basis set zeta is 1")
    call assert_false(gorb%polarized, &
&     "gaufre_orbital_t - orbital is not polarized")
    call assert_equals(10.0_gfdp, gorb%population, &
&     "gaufre_orbital_t - orbital population is 10.0 (fully occupied)")

    call gorb%free()

  end subroutine test_gaufre_orbital_from_siesta_loads_normal_orb

  !> \brief Checks that gaufre_orbital_t%from_siesta properly loads
  !! polarized orbitals
  subroutine test_gaufre_orbital_from_siesta_loads_polarized_orb

    implicit none

    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%from_siesta("gaufre_orbital_test_02.siesta.orb", gfstat=errno)

    call assert_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_orbital_t - from_siesta can read orbital file")
    call assert_true(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - underlying gaufre_data_t is valid")
    call assert_true(gorb%is_valid(), &
&     "gaufre_orbital_t - orbital data is valid")

    call assert_equals("Si ", gorb%species, &
&     "gaufre_orbital_t - species symbol is left-adjusted 'Si '")
    call assert_equals(3, gorb%qn_n, &
&     "gaufre_orbital_t - principal quantum number is 3")
    call assert_equals(2, gorb%qn_l, &
&     "gaufre_orbital_t - angular momentum is 2 (d orbital)")
    call assert_equals(1, gorb%zeta, &
&     "gaufre_orbital_t - basis set zeta is 1")
    call assert_true(gorb%polarized, &
&     "gaufre_orbital_t - orbital is polarized")
    call assert_equals(0.0_gfdp, gorb%population, &
&     "gaufre_orbital_t - orbital population is 0.0")

    call gorb%free()

  end subroutine test_gaufre_orbital_from_siesta_loads_polarized_orb

  ! **************************************************************************
  ! *** Dumper                                                             ***
  ! **************************************************************************

  !> \brief Checks that gaufre_orbital_t%dump minimally works
  subroutine test_gaufre_orbital_dump_works()

    use gaufre_common

    implicit none

    character(len=GAUFRE_ERRMSG_LEN) :: errmsg
    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)
    call gorb%dump("test", gfstat=errno, gfmsg=errmsg)

    call assert_equals(GAUFRE_ERR_OK, errno)
    call assert_equals(0, len_trim(errmsg))

  end subroutine test_gaufre_orbital_dump_works

  !> \brief Checks that gaufre_orbital_t%dump with detailed output works
  subroutine test_gaufre_orbital_dump_detailed_works()

    use gaufre_common

    implicit none

    character(len=GAUFRE_ERRMSG_LEN) :: errmsg
    integer :: errno
    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw)
    call gorb%dump("test", dump_all=.true., gfstat=errno, gfmsg=errmsg)

    call assert_equals(GAUFRE_ERR_OK, errno)
    call assert_equals(0, len_trim(errmsg))

  end subroutine test_gaufre_orbital_dump_detailed_works

  ! **************************************************************************
  ! *** Accessors: get_norm                                                ***
  ! **************************************************************************

  !> \brief Checks that gaufre_orbital_t%get_norm minimally works
  subroutine test_gaufre_orbital_get_norm_works()

    implicit none

    ! Manually calculated fitting parameters
    real(gfdp), parameter :: chk_trial(8) = [ &
&     0.476216610316491, 0.172747238156294, &
&     6.74363142466146, 0.0581393790354891, &
&     0.148768342814151, 0.0777444979179371, &
&     191.69307806675, 0.041461868572999]

    type(gaufre_orbital_t) :: gorb

    call gorb%from_siesta("gaufre_orbital_test_02.siesta.orb")

    call assert_true((gorb%get_residue() > GAUFRE_EPS_MAX), &
&     "gaufre_orbital_t - residue is beyond tolerances before fitting")
    call assert_equals(1.0_gfdp, gorb%get_norm(), 1.0e-3_gfdp, &
&     "gaufre_orbital_t - orbital norm is 1 before fitting")

    call gorb%reset_trial(guess=chk_trial)

    call assert_equals(4, gorb%get_ngfs(), &
&     "gaufre_orbital_t - number of Gaussians is 4 after fitting")
    call assert_true((gorb%get_residue() < 5.0e-2), &
&     "gaufre_orbital_t - residue is within tolerances after fitting")
    call assert_equals(1.0_gfdp, gorb%get_norm(), 1.0e-1_gfdp, &
&     "gaufre_orbital_t - orbital norm is 1 after fitting")

    call gorb%free()

  end subroutine test_gaufre_orbital_get_norm_works

  ! **************************************************************************
  ! *** Validator                                                          ***
  ! **************************************************************************

  !> \brief Checks that gaufre_orbital_t%is_valid works properly
  subroutine test_gaufre_orbital_is_valid_works()

    use gaufre_common

    implicit none

    type(gaufre_orbital_t) :: gorb

    ! The state of the class should not cause the routine to crash
    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid works on uninitialized classes")

    call gorb%init(xraw, yraw)

    call assert_true(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - gaufre_data_t%is_valid works on half-initialized classes")
    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid works on half-initialized classes")

    call gorb%init_physics("Xyz", 3, 2, 1, .false., 2.0_gfdp)

    call assert_true(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - gaufre_data_t%is_valid works on fully-initialized classes")
    call assert_true(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid works on fully-initialized classes")

    call gorb%free()

    call assert_false(gorb%gaufre_data_t%is_valid(), &
&     "gaufre_orbital_t - gaufre_data_t%is_valid works on freed classes")
    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid works on freed classes")

  end subroutine test_gaufre_orbital_is_valid_works

  !> \brief Checks that gaufre_orbital_t%is_valid detects invalid
  !! principal quantum numbers
  subroutine test_gaufre_orbital_is_valid_detects_qn_n()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw, ngfs=3)
    call gorb%init_physics("Xyz", 3, 2, 1, .false., 2.0_gfdp)

    call assert_true(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects valid qn_n")

    gorb%qn_n = -3

    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects invalid qn_n")

    call gorb%free()

  end subroutine test_gaufre_orbital_is_valid_detects_qn_n

  !> \brief Checks that gaufre_orbital_t%is_valid detects invalid
  !! angular momenta
  subroutine test_gaufre_orbital_is_valid_detects_qn_l()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw, ngfs=3)
    call gorb%init_physics("Xyz", 3, 2, 1, .false., 2.0_gfdp)

    call assert_true(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects valid qn_l")

    gorb%qn_l = 3

    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects out-of-bounds qn_l")

    gorb%qn_l = -3

    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects invalid qn_l")

    call gorb%free()

  end subroutine test_gaufre_orbital_is_valid_detects_qn_l

  !> \brief Checks that gaufre_orbital_t%is_valid detects invalid
  !! basis set zetas
  subroutine test_gaufre_orbital_is_valid_detects_zeta()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw, ngfs=3)
    call gorb%init_physics("Xyz", 3, 2, 1, .false., 2.0_gfdp)

    call assert_true(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects valid zeta")

    gorb%zeta = -3

    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects invalid zeta")

    call gorb%free()

  end subroutine test_gaufre_orbital_is_valid_detects_zeta

  !> \brief Checks that gaufre_orbital_t%is_valid detects invalid
  !! principal quantum numbers
  subroutine test_gaufre_orbital_is_valid_detects_population()

    implicit none

    type(gaufre_orbital_t) :: gorb

    call gorb%init(xraw, yraw, ngfs=3)
    call gorb%init_physics("Xyz", 3, 2, 1, .false., 2.0_gfdp)

    call assert_true(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects valid population")

    gorb%population = -3.0_gfdp

    call assert_false(gorb%is_valid(), &
&     "gaufre_orbital_t - is_valid detects invalid population")

    call gorb%free()

  end subroutine test_gaufre_orbital_is_valid_detects_population

end module gaufre_orbital_test
