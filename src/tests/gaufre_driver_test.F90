!! Copyright (C) 2018 Yann Pouillon <devops@materialsevolution.es>
!!
!! This file is part of GAUFRE.
!!
!! This Source Code Form is subject to the terms of the Mozilla Public License,
!! version 2.0. If a copy of the MPL was not distributed with this file, You
!! can obtain one at https://mozilla.org/MPL/2.0/.
!!
!! GAUFRE is distributed in the hope that it will be useful, but WITHOUT ANY
!! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
!! FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
!! more details.

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

module gaufre_driver_test

  use fruit
  use gaufre_common
  use gaufre_driver

  implicit none

  integer, parameter :: GAUFRE_DRIVER_TEST_METHOD = 1

contains

  ! **************************************************************************
  ! *** Constructor and destructor                                         ***
  ! **************************************************************************

  !> \brief Checks that gaufre_driver_t classes are actually invalid before
  !!        initialization
  subroutine test_gaufre_driver_invalid_before_init()

    implicit none

    type(gaufre_driver_t) :: gdrv

    call assert_false(gdrv%is_valid(), &
&     "gaufre_driver_t - class is invalid before init")

  end subroutine test_gaufre_driver_invalid_before_init

  !> \brief Checks that gaufre_driver_t%init produces valid configurations
  !!        by default
  subroutine test_gaufre_driver_valid_after_init()

    implicit none

    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN)
    call assert_true(gdrv%is_valid(), &
&     "gaufre_driver_t - class is valid after init")

  end subroutine test_gaufre_driver_valid_after_init

  !> \brief Checks that gaufre_driver_t%free produces invalid configurations
  subroutine test_gaufre_driver_invalid_after_free()

    implicit none

    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN)
    call gdrv%free()
    call assert_false(gdrv%is_valid(), &
&     "gaufre_driver_t - class is invalid after free")

  end subroutine test_gaufre_driver_invalid_after_free

  !> \brief Checks that init parameters are actually transmitted to the class
  subroutine test_gaufre_driver_init_transmits_params()

    use gaufre_common

    implicit none

    logical :: chk_fm, chk_fn
    integer :: chk_m, chk_n
    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN+1)
    call gdrv%export_internals(method=chk_m, ngfs=chk_n)

    call assert_equals(GAUFRE_DRIVER_TEST_METHOD, chk_m, &
&     "gaufre_driver_t - method is transmitted")
    call assert_equals(GAUFRE_NGFS_MIN+1, chk_n, &
&     "gaufre_driver_t - ngfs is transmitted")

  end subroutine test_gaufre_driver_init_transmits_params

  !> \brief Checks that init detects invalid methods
  subroutine test_gaufre_driver_init_detects_method()

    use gaufre_common

    implicit none

    integer :: chk_m, errno
    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_FIT_UNKNOWN-1, GAUFRE_NGFS_MIN, gfstat=errno)
    call gdrv%export_internals(method=chk_m)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_driver_t - init throws an error for invalid methods (too small)")
    call assert_false(gdrv%is_valid(), &
&     "gaufre_driver_t - init ignores invalid methods (too small)")
    call assert_equals(-1, chk_m, &
&     "gaufre_driver_t - internal method is kept unset (too small)")

    call gdrv%init(huge(1), GAUFRE_NGFS_MIN, gfstat=errno)
    call gdrv%export_internals(method=chk_m)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_driver_t - init throws an error for invalid methods (too large)")
    call assert_false(gdrv%is_valid(), &
&     "gaufre_driver_t - init ignores invalid methods (too large)")
    call assert_equals(-1, chk_m, &
&     "gaufre_driver_t - internal method is kept unset (too large)")

  end subroutine test_gaufre_driver_init_detects_method

  !> \brief Checks that init detects invalid numbers of Gaussian functions
  subroutine test_gaufre_driver_init_detects_ngfs()

    use gaufre_common

    implicit none

    integer :: chk_n, errno
    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN-1, gfstat=errno)
    call gdrv%export_internals(ngfs=chk_n)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_driver_t - init throws an error for invalid methods (too small)")
    call assert_false(gdrv%is_valid(), &
&     "gaufre_driver_t - init ignores invalid ngfs (too small)")
    call assert_equals(-1, chk_n, &
&     "gaufre_driver_t - internal ngfs is kept unset (too small)")

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MAX+1, gfstat=errno)
    call gdrv%export_internals(ngfs=chk_n)

    call assert_not_equals(GAUFRE_ERR_OK, errno, &
&     "gaufre_driver_t - init throws an error for invalid ngfs (too large)")
    call assert_false(gdrv%is_valid(), &
&     "gaufre_driver_t - init ignores invalid ngfs (too large)")
    call assert_equals(-1, chk_n, &
&     "gaufre_driver_t - internal ngfs is kept unset (too large)")

  end subroutine test_gaufre_driver_init_detects_ngfs

  !> \brief Checks that gaufre_driver_t%free resets all fields
  subroutine test_gaufre_driver_free_resets_fields()

    use gaufre_common

    implicit none

    logical :: chk_fm, chk_fn
    integer :: chk_m, chk_n
    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN)
    call gdrv%free()
    call gdrv%export_internals(method=chk_m, ngfs=chk_n)

    call assert_equals(-1, chk_m, &
&     "gaufre_driver_t - method is reset")
    call assert_equals(-1, chk_n, &
&     "gaufre_driver_t - ngfs is reset")

  end subroutine test_gaufre_driver_free_resets_fields

  ! **************************************************************************
  ! *** Dumper                                                             ***
  ! **************************************************************************

  !> \brief Checks that gaufre_driver_t%dump minimally works
  subroutine test_gaufre_driver_dump_works()

    use gaufre_common

    implicit none

    character(len=GAUFRE_ERRMSG_LEN) :: errmsg
    integer :: errno
    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN)
    call gdrv%dump("test", gfstat=errno, gfmsg=errmsg)

    call assert_equals(GAUFRE_ERR_OK, errno)
    call assert_equals(0, len_trim(errmsg))

  end subroutine test_gaufre_driver_dump_works

  !> \brief Checks that gaufre_driver_t%dump with details minimally works
  subroutine test_gaufre_driver_dump_all_works()

    use gaufre_common

    implicit none

    character(len=GAUFRE_ERRMSG_LEN) :: errmsg
    integer :: errno
    type(gaufre_driver_t) :: gdrv

    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN)
    call gdrv%dump("test", dump_all=.true., gfstat=errno, gfmsg=errmsg)

    call assert_equals(GAUFRE_ERR_OK, errno)
    call assert_equals(0, len_trim(errmsg))

  end subroutine test_gaufre_driver_dump_all_works

  ! **************************************************************************
  ! *** Validator                                                          ***
  ! **************************************************************************

  !> \brief Checks that gaufre_driver_t%is_valid works properly
  subroutine test_gaufre_driver_is_valid_works()

    use gaufre_common

    implicit none

    type(gaufre_driver_t) :: gdrv

    ! The state of the class should not cause the routine to crash
    call assert_true(.not. gdrv%is_valid(), &
&     "gaufre_driver_t - is_valid works on uninitialized classes")
    call gdrv%init(GAUFRE_DRIVER_TEST_METHOD, GAUFRE_NGFS_MIN)
    call assert_true(gdrv%is_valid(), &
&     "gaufre_driver_t - is_valid works on initialized classes")
    call gdrv%free()
    call assert_true(.not. gdrv%is_valid(), &
&     "gaufre_driver_t - is_valid works on freed classes")

  end subroutine test_gaufre_driver_is_valid_works

end module gaufre_driver_test
