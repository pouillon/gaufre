#!/bin/bash
#
# Copyright (C) 2018 Yann Pouillon <devops@materialsevolution.es>

# Note: this script is for maintainers and testers working with GCC

# Stop at first error and echo commands
set -ev

# Check that we are in the correct directory
test -s "configure.ac" -a -s "src/gaufre.F90" || exit 0

# Init build parameters
# WARNING: FRUIT fails with -Og or -O1
export DBGFLAGS="-O0 -g3 -ggdb -Wall -Wextra"
export FC_DBGFLAGS="-fbounds-check"

# Prepare source tree
./wipeout.sh
./autogen.sh

# Check default build
# WARNING: MINPACK is not thread-safe
mkdir tmp-minimal
cd tmp-minimal
../configure \
  ${CFGFLAGS} \
  CC="gcc" CFLAGS="${DBGFLAGS}" \
  FC="gfortran" FCFLAGS="${DBGFLAGS} ${FC_DBGFLAGS}"
sleep 3
make dist
make
make check
mkdir install-minimal
make install DESTDIR="${PWD}/install-minimal"
ls -lR install-minimal >install-minimal.log
cd ..

# Update build parameters
unset CFGFLAGS

# Make distcheck
# WARNING: MINPACK is not thread-safe
mkdir tmp-distcheck
cd tmp-distcheck
../configure \
  CC="gcc" \
  FC="gfortran"
sleep 3
make distcheck
make distcleancheck

# Clean-up the mess
cd ..
rm -rf tmp-minimal tmp-distcheck
