bumpversion>=0.5.3
cookiecutter>=1.6.0
flake8>=3.7.8
flit>=1.3
pep8>=1.7.1
pytest>=5.2.0
twine>=1.15.0
