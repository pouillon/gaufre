#!/usr/bin/env python3

import pathlib
import setuptools


HERE = pathlib.Path(__file__).parent
long_description = (HERE / "README.rst").read_text()

setuptools.setup(
    name="PyGAUFRE",
    version="0.6.1",
    author="Yann Pouillon",
    author_email="devops@materialsevolution.es",
    description="Python utilities for the GAUFRE library",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.com/pouillon/gaufre",
    packages=setuptools.find_packages(exclude=("tests",)),
    include_package_data=True,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Scientific/Engineering :: Chemistry",
        "Topic :: Scientific/Engineering :: Physics",
        "Topic :: Scientific/Engineering :: Visualization",
    ],
    project_urls={
        "Documentation": "https://gitlab.com/pouillon/gaufre/tree/master/doc",
        "Source": "https://gitlab.com/pouillon/gaufre",
        "Tracker": "https://gitlab.com/pouillon/gaufre/issues",
    },
    keywords="gaussian electron fitting orbital wavefunction",
    python_requires="~=3.4",
    entry_points={
        "console_scripts": [
            "gaufre_fit_orbitals = gaufre.cli.fitter:gen_fit_data",
            "gaufre_show_orbitals = gaufre.cli.viewer:plot_fit_data",
        ],
    },
)
