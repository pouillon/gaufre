#!/usr/bin/python3

import argparse
import os
import sys
import time
import traceback

import gaufre.io.readers as rdrs

from gaufre.fitters.altern import AlternFitter
from gaufre.fitters.basinhopping import BasinHoppingFitter
from gaufre.fitters.diffevol import DifferentialEvolutionFitter
from gaufre.fitters.pinv import PseudoinverseFitter
from gaufre.fitters.segment import SegmentFitter
from gaufre.io.writers import FdfNao2gtoWriter
from gaufre.orbitals.siesta import SiestaOrbital


                    # ------------------------------------ #


def gen_fit_data():

    # Define program options
    parser = argparse.ArgumentParser()
    parser.add_argument("location",
        help="Location of the orbital data (can be a directory with ORB.* files, a .ion.xml file, or a GAUFRE YAML file)")
    parser.add_argument("outfile",
        help="Output file to store the Gaussian expansion")
    parser.add_argument("--bounds", action="store_true",
        help="Enforce boundaries on Gaussian exponents")
    parser.add_argument("--method",
        choices=["altern", "de", "hopping", "pinv", "segment"],
        default="segment",
        help="Fitting method to apply")
    parser.add_argument("--nsfs", type=float, default=3.0,
        help="Number of Gaussian sigmas to ensure in frequency space")
    parser.add_argument("--nsrs", type=float, default=3.0,
        help="Number of Gaussian sigmas to ensure in real space")
    parser.add_argument("--overwrite", action="store_true",
        help="Overwrite existing output files")
    parser.add_argument("--ratio", type=float, default=100.0,
        help="Ratio of importance of the residues with respect to the Guassian coefficients")
    parser.add_argument("--verbose", action="store_true",
        help="Display information about what the program is doing")
    args = parser.parse_args()

    # Select files to read and extract orbital information
    siesta_orbs = []
    siesta_trials = None
    if ( not os.path.exists(args.location) ):
        parser.error("File not found: {}".format(args.location))
    if ( os.path.isdir(args.location) ):
        orb_species = [item.split(".")[-1] for item in os.listdir(args.location) \
            if ( item.startswith("ORB.") )]
        if ( len(orb_species) == 0 ):
            parser.error("Orbital files not found: ORB.*")
        orb_paths = sorted([os.path.join(args.location, item) \
            for item in os.listdir(args.location) \
            if ( item.startswith("ORB.") )])
        siesta_orbs = list(rdrs.RawOrbReader(orb_paths))
    elif ( args.location.endswith(".ion.xml") ):
        siesta_orbs = list(rdrs.IonXmlReader(args.location))
    elif ( args.location.endswith(".yaml") or args.location.endswith(".yml") ):
        siesta_orbs = list(rdrs.YamlReader(args.location))
    else:
        data_fmt = args.location.split(".")
        if ( len(data_fmt) == 1 ):
            data_fmt =  "unknown"
        else:
            data_fmt = data_fmt[-1]
        parser.error("No reader available for the {} file format".format(
            data_fmt))

    # Save the actual fitting parameters
    fit_info = "# gaufre-fit-orbitals \\\n#   --ratio={r:.3e}".format(
        r=args.ratio)
    if ( args.bounds ):
        fit_info += "\\\n#   --bounds \\\n" \
            + "#   --nsfs={f:.3e} \\\n#   --nsrs={r:.3e}".format(
                f=args.nsfs, r=args.nsrs)
    fit_info += "\n#\n"
    if ( args.verbose ):
        print("gaufre-fit-orbitals")
        print("")
        print("    --ratio = {x:.3e}".format(x=args.ratio))
        if ( args.bounds ):
            print("    --bounds")
            print("    --nsfs  = {f:.3e}".format(f=args.nsfs))
            print("    --nsrs  = {r:.3e}".format(r=args.nsrs))
        print("")

    # Fit the specified SIESTA orbitals
    if ( args.verbose ):
        print("Will fit {norb} orbitals\n".format(norb=len(siesta_orbs)))
    for orb in siesta_orbs:
        if ( args.verbose ):
            print("Orbital ({n},{l},{z}): start".format(n=orb.qn_n,
                l=orb.qn_l, z=orb.zeta))
        clock1 = time.perf_counter()
        res_min = 100.0*orb.get_norm()

        # Note: some methods do not work with 1 Gaussian
        for ngfs in range(6,2,-1):
            if ( args.method == "altern" ):
                fitter = AlternFitter(ngfs, orb.xraw, orb.yraw)
            elif ( args.method == "de" ):
                fitter = DifferentialEvolutionFitter(orb.xraw, orb.yraw)
            elif ( args.method == "hopping" ):
                fitter = BasinHoppingFitter(orb.xraw, orb.yraw)
            elif ( args.method == "pinv" ):
                fitter = PseudoinverseFitter(orb.xraw, orb.yraw, args.ratio)
            elif ( args.method == "segment" ):
                fitter = SegmentFitter(ngfs, orb.xraw, orb.yraw)
            else:
                raise ValueError("Invalid fitting method: '{}'".format(
                    args.method))

            try:
                trial, res_try = fitter.seek_fit(bounds=args.bounds,
                    nsfs=args.nsfs, nsrs=args.nsrs, verbose=args.verbose)
            except:
                etype, evalue, tb = sys.exc_info()
                print("Warning: the fitter raised an exception")
                traceback.print_exception(etype, evalue, tb)
                continue
            if ( res_try < res_min ):
                orb.set_fit(trial)
                res_min = res_try
            if ( args.verbose ):
                print("Orbital ({n},{l},{z}): {g} Gaussians, residue = {r:.3e}".format(
                    n=orb.qn_n, l=orb.qn_l, z=orb.zeta, g=ngfs, r=res_try))
        clock2 = time.perf_counter()
        if ( args.verbose ):
            for a, b in orb.get_fit():
                print("{:15.5e} {:15.5e}".format(a, b))
        fit_info += "# Orbital ({n},{l},{z}): {g} Gaussians, residue = {r:.3e}, elapsed {t:.3e}s\n".format(
            n=orb.qn_n, l=orb.qn_l, z=orb.zeta, g=orb.get_ngfs(),
            r=res_min, t=clock2-clock1)

    # Write down a FDF description of the fitting parameters
    writer = FdfNao2gtoWriter(siesta_orbs)
    if ( args.verbose ):
        writer.print_fdf(fdf_header=fit_info)
    writer.write_fdf(args.outfile, fdf_header=fit_info, overwrite=args.overwrite)


                    # ------------------------------------ #


if ( __name__ == "__main__" ):
    gen_fit_data()
