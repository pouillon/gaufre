import argparse
import os

import numpy
import matplotlib.pyplot as plt

import gaufre.io.readers as rdrs

from gaufre.cli.pathtype import PathType
from gaufre.orbitals.siesta import SiestaOrbital


                    # ------------------------------------ #


def plot_fit_data():

    # Define program options
    parser = argparse.ArgumentParser()
    parser.add_argument("location",
        help="Location of the orbital data (can be a directory with ORB.* files, a .ion.xml file, or a GAUFRE YAML file)")
    parser.add_argument("-a", "--aux",
        type=PathType(type="file", exists=True, dash_ok=False),
        help="Auxiliary FDF file containing fitting parameters")
    parser.add_argument("-d", "--diff",
        action="store_true",
        help="Show fitting residuals instead of orbitals")
    parser.add_argument("-s", "--species",
        help="Species to select")
    args = parser.parse_args()

    # Select files to read and extract orbital information
    orb_species = args.species
    siesta_orbs = []
    siesta_trials = None
    if ( not os.path.exists(args.location) ):
        parser.error("File not found: {}".format(args.location))
    if ( os.path.isdir(args.location) ):
        if ( args.species ):
            orb_paths = sorted([os.path.join(args.location, item) \
                for item in os.listdir(args.location) \
                if ( item.startswith("ORB.") and item.endswith(args.species) )])
            if ( len(orb_paths) == 0 ):
                parser.error("Orbital files not found:\n    {}{}ORB.*.{}".format(
                    args.prefix, os.pathsep, args.species))
        else:
            orb_species = [item.split(".")[-1] for item in os.listdir(args.location) \
                if ( item.startswith("ORB.") )]
            orb_species = list(set(orb_species))
            if ( len(orb_species) == 0 ):
                parser.error("Orbital files not found: ORB.*")
            if ( len(orb_species) > 1 ):
                parser.error("Please select only one species among {} with the -s/--species option".format(", ".join(orb_species)))
            orb_paths = sorted([os.path.join(args.location, item) \
                for item in os.listdir(args.location) \
                if ( item.startswith("ORB.") )])
            orb_species = orb_species[0]
        siesta_orbs = list(rdrs.RawOrbReader(orb_paths))
    elif ( args.location.endswith(".ion.xml") ):
        siesta_orbs = list(rdrs.IonXmlReader(args.location))
    elif ( args.location.endswith(".yaml") or args.location.endswith(".yml") ):
        siesta_orbs = list(rdrs.YamlReader(args.location))
    else:
        data_fmt = args.location.split(".")
        if ( len(data_fmt) == 1 ):
            data_fmt =  "unknown"
        else:
            data_fmt = data_fmt[-1]
        parser.error("No reader available for the {} file format".format(data_fmt))

    # Decide whether to use available auxiliary information
    aux_path = None
    if ( args.aux ):
        aux_path = args.aux
    else:
        if ( os.path.isdir(args.location) ):
            aux_dir = args.location
        else:
            aux_dir = os.path.dirname(args.location)
        if ( os.path.exists(os.path.join(aux_dir, "nao2gto.fdf")) ):
            aux_path = os.path.join(aux_dir, "nao2gto.fdf")
    if ( not aux_path is None ):
        siesta_trials = rdrs.FdfNao2gtoReader(aux_path)

    # Make sure orb_species is defined
    if ( orb_species is None ):
        orb_species = siesta_orbs[0].species

    # Explicitly add trial vectors when relevant
    if ( siesta_trials ):
        for iorb in range(len(siesta_orbs)):
            species = siesta_orbs[iorb].species
            qn_n = siesta_orbs[iorb].qn_n
            qn_l = siesta_orbs[iorb].qn_l
            zeta = siesta_orbs[iorb].zeta
            trial = siesta_trials.get_trial(species, qn_n, qn_l, zeta)
            siesta_orbs[iorb].set_fit(trial)

    # Set the shape of the plot
    norbs = len(siesta_orbs)
    if ( norbs == 0 ):
        parser.error("Orbitals are not initialized")
    cols = max(1, int(numpy.sqrt(norbs)))
    if ( norbs > cols**2 ):
      cols += 1
    rows = norbs // cols
    if ( norbs > cols*rows ):
      rows += 1

    # Prepare the figure
    plt.rcParams["figure.figsize"] = [2*cols+12, 4*rows+1]
    fig, ax = plt.subplots(rows, cols)
    if ( rows == 1 ):
        ax = [ax]
        if ( cols == 1 ):
            ax = [ax]

    plt.subplots_adjust(top=0.90, bottom=0.10, wspace=0.35, hspace=0.50)
    fig.suptitle("SIESTA orbitals of {}".format(orb_species), fontsize=14)

    # Plot orbitals
    for irow in range(rows):
        for icol in range(cols):
            iorb = irow*cols + icol
            if ( iorb < norbs ):
                orb = siesta_orbs[iorb]
                orb.make_plot(ax[irow][icol], residual=args.diff)
                raw_norm = orb.get_norm()
                if ( siesta_orbs[iorb].has_fit() ):
                    fit_norm = orb.get_norm(fit=True)
                    print("NORM({:2s}, n={:d},l={:d},z={:d}): raw = {:.5e}, fit = {:.5e} (RES = {:.5e})".format(
                        orb.species, orb.qn_n, orb.qn_l, orb.zeta,
                        raw_norm, fit_norm, orb.get_residue()))
                else:
                    print("NORM({:2s}, n={:d},l={:d},z={:d}): raw = {:.5e}".format(
                        orb.species, orb.qn_n, orb.qn_l, orb.zeta, raw_norm))
            else:
                ax[irow][icol].axis("off")
    plt.show()


                    # ------------------------------------ #


if ( __name__ == "__main__" ):
    plot_fit_data()
