"""Python utilities for GAUFRE

PyGAUFRE is a set of Python modules and scripts to process, visualize and
validate fitting data produced by the GAUFRE library."""

__version__ = "0.6.1"
