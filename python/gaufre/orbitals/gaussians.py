#!/usr/bin/python3

import numpy


                    # ------------------------------------ #


class GaussianOrbital(object):


    def __init__(self, species, qn_n, qn_l, zeta, polarized, population,
            coeffs):

        self.species = species
        self.qn_n = qn_n
        self.qn_l = qn_l
        self.zeta = zeta
        self.polarized = polarized
        self.population = population
        self.coeffs = coeffs


    def get_sigmas(self, xraw=None):

        alphas, betas = zip(*self.coeffs)
        alphas = sorted(alphas)
        sigmas = numpy.zeros(len(alphas))
        for igfs in range(len(alphas)):
            sigmas[igfs] = 1.0 / numpy.sqrt(2.0*alphas[igfs])

        sig_dens = None
        if ( not xraw is None ):
            sig_dens = numpy.zeros_like(xraw)
            sig_span = max(sigmas) - min(sigmas)
            smear =  max(max(xraw)/len(xraw), 5.0*sig_span/len(xraw))
            for sig_mu in sigmas:
                for ipts in range(len(xraw)):
                    sig_dens[ipts] += \
                        numpy.exp(-0.5*(((xraw[ipts] - sig_mu) / smear)**2))

        return sigmas, sig_dens


    def get_values(self, xpts):

        ypts = numpy.zeros(len(xpts))
        for ipts in range(len(xpts)):
            for alpha, beta in self.coeffs:
                ypts[ipts] += beta*numpy.exp(-alpha*xpts[ipts]*xpts[ipts])

        return ypts

