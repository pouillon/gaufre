#!/usr/bin/env python3

import numpy

from matplotlib import pyplot

from gaufre.orbitals.gaussians import GaussianOrbital


                    # ------------------------------------ #


class SiestaOrbital(object):


    def __init__(self, species, qn_n, qn_l, zeta, polarized, population,
            points, trial=[]):

        self.species = species
        self.qn_n = qn_n
        self.qn_l = qn_l
        self.zeta = zeta
        self.polarized = polarized
        self.population = population

        xraw, yraw = zip(*points)
        self.xraw = numpy.array(xraw)
        self.yraw = numpy.array(yraw)

        self.bbox = [min(self.xraw), 1.05*max(self.xraw), min(self.yraw),
            max(self.yraw)]
        self.bbox[2] = (0.05-numpy.sign(self.bbox[2]))*self.bbox[2]
        self.bbox[3] = (0.05+numpy.sign(self.bbox[3]))*self.bbox[3]

        self.sigmas = []
        self.sig_dens = []
        self.yfit = numpy.zeros_like(self.yraw)

        self.trial = []
        self.set_fit(trial)


    @classmethod
    def from_siesta_orb(cls, orb_path):

        orb_data = []
        with open(orb_path, "r") as orb_file:
            orb_data = orb_file.read().strip().split("\n")

        species, qn_l, qn_n, zeta, polarized, population = \
            orb_data[0][1:].split()
        points = []
        for line in orb_data[2:]:
            points.append([float(item) for item in line.split() \
                if len(line.split()) == 2])

        sorb = cls(species.strip(), int(qn_n), int(qn_l), int(zeta),
            bool(int(polarized)), float(population), points)

        return sorb


    @classmethod
    def from_gaussian(cls, gorb, rcut, npts=500):

        if ( rcut < numpy.finfo(float).eps ):
            raise ValueError("Cutoff radius too small: {:%.5e}".format(rcut))

        if ( (npts == 0) or (npts < 4*len(gorb.coeffs)) ):
            raise ValueError("Number of data points too small: {}".format(npts))

        xraw = numpy.linspace(0.0, rcut, npts)
        yraw = gorb.get_values(xraw)
        points = zip(xraw, yraw)

        sorb = cls(gorb.species, gorb.qn_n, gorb.qn_l, gorb.zeta,
            gorb.polarized, gorb.population, points)

        return sorb


    def get_fit(self):

        return self.trial


    def get_ngfs(self):

        return len(self.trial)


    def get_norm(self, fit=False):

        norm = 0.0
        nraw = self.xraw**(2*(self.qn_l+1))
        if ( fit ):
            if ( self.has_fit() ):
                nraw *= self.yfit**2
            else:
                return norm
        else:
            nraw *= self.yraw**2
        norm = numpy.trapz(nraw, self.xraw)

        return norm


    def get_resfunc(self):

        return self.yfit - self.yraw


    def get_residue(self):

        return numpy.sqrt(numpy.sum(self.get_resfunc()**2))


    def has_fit(self):

        return ( len(self.trial) > 0 )


    def import_fit(self, fdf_path):

        fdf_block = []
        fdf_data = []
        trial = []

        with open(fdf_path, "r") as fdf_file:
            fdf_data = fdf_file.read().strip().split("\n")

        fdf_beg = [i for i, s in enumerate(fdf_data) \
            if s.startswith("%block NAO2GTO")][0]
        fdf_end = [i for i, s in enumerate(fdf_data) \
            if s.startswith("%endblock NAO2GTO")][0]
        if ( fdf_end > fdf_beg + 1 ):
            fdf_block = fdf_data[fdf_beg+1:fdf_end]

        offset = 0
        while ( offset < len(fdf_block) ):
            species, norbs = fdf_block[offset].strip().split()[0:2]
            norbs = int(norbs)
            offset += 1
            for iorb in range(norbs):
                qn_n, qn_l, zeta, ngfs = map(int, fdf_block[offset].split())
                if ( (species == self.species) and (qn_n == self.qn_n) and
                     (qn_l == self.qn_l) and (zeta == self.zeta) ):
                    offset += 1
                    for igfs in range(ngfs):
                        trial.append([float(item) \
                            for item in fdf_block[offset].split() \
                            if len(fdf_block[offset].split()) == 2])
                        offset += 1
                else:
                    offset += ngfs + 1

        self.set_fit(trial)


    def make_plot(self, ax, residual=False):

        # Set global plot parameters
        ax.set_title("{:s}: n={:d}, l={:d}, $\zeta$={:d}, pol={:d}, pop={:.3e}\nngfs={:d}, res={:.3e}".format(
            self.species, self.qn_n, self.qn_l, self.zeta, int(self.polarized),
            self.population, len(self.trial), self.get_residue()))
        ax.set_xlabel('$r$ (Bohr)')
        ax.set_ylabel('$\\frac{R_{nl}(r)}{r^l}$', fontsize=20)
        ax.axhline(color="black")

        # Show cutoff radius
        ax.axvspan(max(self.xraw), 2.0*max(self.xraw),
            color="orange", alpha=0.7)

        if ( residual ):

            # Plot the residual fitting function
            resfunc = self.get_resfunc()
            ax.plot(self.xraw, resfunc, "r-", label="resfunc")

            # Adjust view
            ax.set_xlim(left=self.bbox[0], right=self.bbox[1])

        else:

            # Plot fit when available
            if ( len(self.trial) > 0 ):
                ax.vlines(max(self.xraw)/3.0, self.bbox[2], self.bbox[3],
                    colors="red", linestyles="solid")
                ax.vlines(self.sigmas, self.bbox[2], self.bbox[3],
                    colors="magenta", linestyles="dashed")
                ax.plot(self.xraw, self.sig_dens, "k:")
                ax.plot(self.xraw, self.yfit, "g-", label="fit")

            # Plot raw data
            ax.plot(self.xraw, self.yraw, "bo", label="raw",
                markevery=max(1, len(self.xraw)//25))

            # Adjust view
            ax.set_xlim(left=self.bbox[0], right=self.bbox[1])
            ax.set_ylim(bottom=self.bbox[2], top=self.bbox[3])

        # Adjust display properties
        ax.grid()
        ax.legend(loc="upper right")


    def set_fit(self, trial):

        if ( len(trial) > 0 ):
            self.trial = trial

            gorb = GaussianOrbital(self.species, self.qn_n, self.qn_l,
                self.zeta, self.polarized, self.population, trial)
            self.sigmas, self.sig_dens = gorb.get_sigmas(self.xraw)
            self.yfit = gorb.get_values(self.xraw)

            self.bbox[2] = min(min(self.yraw), min(self.yfit))
            self.bbox[3] = max(max(self.yraw), max(self.yfit))

            ysig = 10.0**(int(numpy.floor(numpy.log10(abs(self.bbox[3])))))
            if ( ysig > 0.5*abs(self.bbox[3]) ):
                ysig /= 10.0
            self.sig_dens *= ysig

            self.bbox[2] = (0.05-numpy.sign(self.bbox[2]))*self.bbox[2]
            self.bbox[3] = (0.05+numpy.sign(self.bbox[3]))*self.bbox[3]

