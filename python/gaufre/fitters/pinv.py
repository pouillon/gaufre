#!/usr/bin/env python3

import os
import sys

import numpy as np
from scipy.optimize import lsq_linear, minimize


                    # ------------------------------------ #


class PseudoinverseFitter(object):


    def __init__(self, xraw, yraw, fit_ratio=100.0):

        assert(len(xraw) > 10)
        assert(len(xraw) == len(yraw))
        assert(xraw[-1] > np.finfo(np.float64).eps)
        assert(abs(yraw[-1]) < np.finfo(np.float64).eps)
        assert(fit_ratio > np.finfo(np.float64).eps)

        # Import raw data
        self.xraw = xraw
        self.yraw = yraw
        self.ratio = fit_ratio

        # Init problem dimensions
        # Note: rcut is not always the last point
        self.npts = len(self.xraw)
        self.rcut = self.xraw[-1]
        for ipts in range(self.npts-1, 1, -1):
            if ( abs(yraw[ipts]) > np.finfo(np.float64).eps ):
                break
            self.rcut = self.xraw[ipts]


    def seek_fit(self, ngfs, bounds=False, nsfs=3.0, nsrs=2.5, verbose=False):

        assert(ngfs > 0)
        assert(nsfs > np.finfo(np.float64).eps)
        assert(nsrs > np.finfo(np.float64).eps)

        # Define P vector = factors to explore 2*pi radians with ngfs steps
        m_pi = 4.0*np.arctan(1.0)
        pvec = [np.float64(i)*m_pi/self.rcut for i in range(2*ngfs)]

        # Define T vector = cos(pi*k*x)*f(x), k=0,2*ngfs-1
        tvec = np.zeros(2*ngfs)
        for k in range(2*ngfs):
            for ipts in range(self.npts):
                tvec[k] += np.cos(pvec[k]*self.xraw[ipts])*self.yraw[ipts]
            tvec[k] *= self.rcut/self.npts

        # Define B vector = Gaussian exponents
        bvec = np.ones(ngfs)
        for igfs in range(ngfs):
            bvec[igfs] = 1.0/np.float64(igfs+1)

        # Find a B vector minimizing the fitting function defined below
        if ( bounds ):
            bmin = self.rcut*nsfs*np.sqrt(2.0)/np.float64(self.npts - 1)
            bmax = self.rcut*np.sqrt(2.0)/nsrs
            limits = [(bmin, bmax) for item in bvec]
            result = minimize(self._fit_func, bvec, args=(pvec, tvec),
                method="L-BFGS-B", bounds=limits)
        else:
            result = minimize(self._fit_func, bvec, args=(pvec, tvec),
                method="L-BFGS-B")
        bfin = result.x

        # Construct the corresponding Gaussian coefficients
        bmat = np.zeros((2*ngfs, ngfs))
        for i in range(2*ngfs):
            for j in range(ngfs):
                bmat[i][j] = 0.5*np.sqrt(m_pi) \
                           * bfin[j]*np.exp(-(0.5*pvec[i]*bfin[j])**2)
        cfin = np.matmul(np.linalg.pinv(bmat), tvec)

        # Translate the final result
        trial = []
        for igfs in range(ngfs):
            trial.append([np.float64(1.0)/(bfin[igfs]**2), cfin[igfs]])
        trial.sort(key=lambda x: x[0])

        return trial, self._residue(bfin, cfin)


    def _fit_func(self, bvec, pvec, tvec):

        # Get number of Gaussians
        ngfs = len(bvec)

        # Define Beta matrix = coupled equations to resolve
        m_pi = 4.0*np.arctan(1.0)
        bmat = np.zeros((2*ngfs, ngfs))
        for i in range(2*ngfs):
            for j in range(ngfs):
                bmat[i][j] = 0.5*np.sqrt(m_pi) \
                           * bvec[j]*np.exp(-(0.5*pvec[i]*bvec[j])**2)

        # Define C vector = Gaussian coefficients
        cvec = np.matmul(np.linalg.pinv(bmat), tvec)

        return np.linalg.norm(cvec)/self.ratio + self._residue(bvec, cvec)


    def _fit_point(self, rad, bvec, cvec):

        yfit = 0.0
        for igfs in range(len(bvec)):
            yfit += cvec[igfs] * np.exp(-((rad/bvec[igfs])**2))

        return yfit


    def _residue(self, bvec, cvec):

        ngfs = len(bvec)
        rinc = (self.xraw[-1] - self.xraw[0])/(self.npts - 1)
        nsig = 1 + int(6.0*max(bvec)/np.sqrt(2.0)/rinc)

        yres = np.concatenate((
            np.array([self.yraw[ipts] - \
                self._fit_point(self.xraw[ipts], bvec, cvec) \
                for ipts in range(self.npts)]),
            np.array([self._fit_point(self.rcut + isig*rinc, bvec, cvec) \
                for isig in range(self.npts, max(nsig, self.npts))])))

        return np.linalg.norm(yres)


    def _residue_notail(self, bvec, cvec):

        yres = np.array([self.yraw[ipts] - \
            self._fit_point(self.xraw[ipts], bvec, cvec) \
            for ipts in range(self.npts)])

        return np.linalg.norm(yres)

