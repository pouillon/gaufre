#!/usr/bin/env python3

import os
import sys

import numpy as np
from scipy.optimize import lsq_linear, minimize


                    # ------------------------------------ #


class AlternFitter(object):


    def __init__(self, ngfs, xraw, yraw, fit_ratio=10.0):

        assert(ngfs > 2)
        assert(len(xraw) > 10)
        assert(len(xraw) == len(yraw))
        assert(xraw[-1] > np.finfo(np.float64).eps)
        assert(abs(yraw[-1]) < np.finfo(np.float64).eps)
        assert(fit_ratio > np.finfo(np.float64).eps)

        # Init problem dimensions
        # Note: rcut is not always the last point
        self.ngfs = ngfs
        self.ratio = fit_ratio
        self.npts = len(xraw)
        self.rcut = xraw[-1]
        for ipts in range(len(xraw)-1, 1, -1):
            if ( abs(yraw[ipts]) > np.finfo(np.float64).eps ):
                break
            self.npts = ipts
            self.rcut = xraw[ipts]
        self.rinc = (self.rcut - xraw[0])/(self.npts - 1)
        assert(self.npts > 0.5*len(xraw))

        # Import raw data
        self.xraw = xraw[:self.npts]
        self.yraw = yraw[:self.npts]

        # Define P vector = factors to explore 2*pi radians with ngfs steps
        m_pi = 4.0*np.arctan(1.0)
        self.r_pi = np.sqrt(m_pi)
        self.pvec = [np.float64(i)*m_pi/self.rcut for i in range(2*self.ngfs)]

        # Define T vector = cos(pi*k*x)*f(x), k=0,2*ngfs-1
        self.tvec = np.zeros(2*self.ngfs)
        for k in range(2*self.ngfs):
            for ipts in range(self.npts):
                self.tvec[k] += np.cos(self.pvec[k]*self.xraw[ipts]) \
                              * self.yraw[ipts]
            self.tvec[k] *= self.rcut/self.npts


    def seek_fit(self, bounds=False, nsfs=3.0, nsrs=2.5, verbose=False):

        assert(nsfs > np.finfo(np.float64).eps)
        assert(nsrs > np.finfo(np.float64).eps)

        # Define initial B vector = Gaussian exponents
        bvec = np.linspace(1.0, 0.5*np.float64(self.ngfs), self.ngfs)

        # Set minimization criteria for the B vector
        gtol = 1.0e-7
        gh = 1.0e-4
        gmaxiter = 100
        bnew, berr = self._min_grad(bvec, gtol, gmaxiter)
        cnew = self._build_cvec(bnew)
        ferr = self._fit_func(bnew)
        if ( verbose ):
            print("B   =", bnew)
            print("ERR =", ferr)

        # Perform an alternating optimization with the current crieria
        ntry_max = 400
        ferr_tol = 1.0e-3
        derr_tol = 1.0e-6
        derr = 1.0
        itry = 0
        while ( (itry < ntry_max) and (ferr > ferr_tol) and \
                (abs(derr) > derr_tol) ):
            itry += 1
            cnew = self._quad_min(bnew)
            bnew, ferr_new = self._min_grad(bnew, gtol, gmaxiter)
            derr = abs(ferr_new - ferr)
            ferr = ferr_new
            if ( itry % 10 == 0 ):
                print("{:6d} NGFS={:d}, ERR={:.3e}, GRAD={:.3e}".format(
                    itry, self.ngfs, ferr, derr))
        cnew = self._quad_min(bnew)

        # Translate the final result
        trial = []
        for igfs in range(self.ngfs):
            trial.append([np.float64(1.0)/(bnew[igfs]**2), cnew[igfs]])
        trial.sort(key=lambda x: x[0])

        return trial, ferr


    def _build_cvec(self, bvec):

        # Define Beta matrix = coupled equations to resolve
        m_pi = 4.0*np.arctan(1.0)
        bmat = np.zeros((2*self.ngfs, self.ngfs))
        for i in range(2*self.ngfs):
            for j in range(self.ngfs):
                bmat[i][j] = 0.5*np.sqrt(m_pi) \
                           * bvec[j]*np.exp(-(0.5*self.pvec[i]*bvec[j])**2)

        # Define C vector = Gaussian coefficients
        try:
            cvec = np.matmul(np.linalg.pinv(bmat), self.tvec)
        except np.linalg.linalg.LinAlgError as err:
            print("Calculating the pseudoinverse with an alternate method")
            result = lsq_linear(bmat, self.tvec)
            cvec = result.x

        return cvec


    def _fit_func(self, bvec):

        cvec = self._build_cvec(bvec)

        return np.sqrt((np.linalg.norm(cvec)/self.ratio)**2 + \
            self._residue(bvec, cvec)**2)


    def _fit_point(self, rad, bvec, cvec):

        yfit = 0.0
        for igfs in range(self.ngfs):
            yfit += cvec[igfs] * np.exp(-((rad/bvec[igfs])**2))

        return yfit


    def _min_grad(self, bvec, gtol, gmaxiter):
        """Find a local minimum of _fit_func using the gradient method,
           starting at bvec, using gtol as tolerance and performing a
           maximum number of iterations equal to gmaxiter."""

        # Set initial parameters
        bmin = np.array(bvec, copy=True)
        berr = 1.0
        niter = 0
        h0 = self._fit_func(bvec)

        while ( (berr > gtol) and (niter < gmaxiter) ):
            niter += 1

            # Calculate gradient
            bgrad = np.zeros(self.ngfs)
            for j in range(self.ngfs):
                ej = np.zeros(self.ngfs)
                ej[j] = 1.0
                bgrad[j] = 0.5*(self._fit_func(bmin+gtol*ej) - \
                    self._fit_func(bmin-gtol*ej))/gtol
            berr = np.linalg.norm(bgrad)
            bgrad /= berr

            # Now that we have the gradient (unitary) at the current point,
            # we look for alpha^* verifying the above conditions
            bmin = sorted(np.abs(bmin))
            mindist = min(np.diff(bmin))
            alphastar = min(1.0, 0.25*mindist/bmin[-1])
            halphastar = self._fit_func(bmin-bgrad)
            hiter = 0
            while ( (halphastar > h0) and (hiter < gmaxiter) ):
                hiter += 1
                alphastar *= 0.5
                halphastar = self._fit_func(bmin-alphastar*bgrad)

            # Now that we have alpha^*, we look for the optimal value for
            # alpha
            frac_num = halphastar - \
                4.0*self._fit_func(bmin-0.5*alphastar*bgrad) + \
                3.0*h0
            frac_den = 4.0*halphastar - \
                8.0*self._fit_func(bmin-0.5*alphastar*bgrad) + \
                4.0*h0
            alpha1 = halphastar*frac_num/frac_den
            halpha1 = self._fit_func(bmin+alpha1*bgrad)
            if ( halpha1 < halphastar ):
                alphastar = alpha1
                h0 = halpha1
            else:
                h0 = halphastar

            # Now we can continue knowing the optimal step size
            bmin = bmin - alphastar*bgrad

            #if ( niter % 10 == 0 ):
            #    print("GRAD {:6d} NGFS={:d}, ALPHA={:.3e}, H0={:.3e}, ERR={:.3e}".format(
            #        niter, self.ngfs, alphastar, h0, berr))

        return bmin, self._fit_func(bmin)


    def _quad_min(self, bvec):

        bmqm = np.zeros((self.npts, self.ngfs))
        for ipts in range(self.npts):
            for igfs in range(self.ngfs):
                bmqm[ipts][igfs] = np.exp(-(self.xraw[ipts]/bvec[igfs])**2)

        return np.linalg.lstsq(bmqm, self.yraw, rcond=-1)[0]


    def _residue(self, bvec, cvec):

        nsig = 1 + int(6.0*max(bvec)/np.sqrt(2.0)/self.rinc)
        yres = np.concatenate((
            np.array([self.yraw[ipts] - \
                self._fit_point(self.xraw[ipts], bvec, cvec) \
                for ipts in range(self.npts)]),
            np.array([self._fit_point(self.rcut + isig*self.rinc, bvec, cvec) \
                for isig in range(self.npts, max(nsig, self.npts))])))

        return np.linalg.norm(yres)


    def _residue_notail(self, bvec, cvec):

        yres = np.array([self.yraw[ipts] - \
            self._fit_point(self.xraw[ipts], bvec, cvec) \
            for ipts in range(self.npts)])

        return np.linalg.norm(yres)

