#!/usr/bin/env python3

import os
import sys

import numpy as np
from scipy.optimize import differential_evolution, minimize


                    # ------------------------------------ #


class DifferentialEvolutionFitter(object):


    def __init__(self, xraw, yraw):

        assert(len(xraw) > 10)
        assert(len(xraw) == len(yraw))
        assert(xraw[-1] > np.finfo(np.float64).eps)

        # Import raw data
        self.xraw = xraw
        self.yraw = yraw

        # Init problem dimensions
        self.npts = len(self.xraw)
        self.rcut = self.xraw[-1]


    def seek_fit(self, ngfs, bounds=False, nsfs=3.0, nsrs=2.5, verbose=False):

        assert(ngfs > 0)
        assert(nsfs > np.finfo(np.float64).eps)
        assert(nsrs > np.finfo(np.float64).eps)

        # Set the permitted value ranges
        if ( bounds ):
            amin = 0.5*((nsrs/self.rcut)**2)
            amax = 0.5*((np.float64(self.npts - 1)/(nsfs*self.rcut))**2)
        else:
            amin = 0.0
            amax = 1.06
        limits = []
        for igfs in range(ngfs):
            limits.append([amin, amax])
            limits.append([-1.0e3, 1.0e3])

        # Find an initial set of candidates
        pop_init = []
        for ipop in range(16):
            if ( verbose ):
                print("Creating initial trial vector #{:02d}".format(ipop+1))
            trial = np.random.rand(2*ngfs)
            res = minimize(self._residue, trial, bounds=limits,
                method="L-BFGS-B", tol=1.5e-1)
            pop_init.append(res.x)

        # Minimize the residue by differential evolution
        result = differential_evolution(self._residue, bounds=limits,
            strategy="best1bin", init=pop_init, atol=1.0-2, disp=verbose,
            workers=4)
        trial = result.x
        print("Success:", result.success)
        print("Message:", result.message)

        # Translate the final result
        ferr = self._residue(trial)
        coeffs = [[trial[igfs], trial[igfs+1]] \
            for igfs in range(0, len(trial), 2)]
        coeffs.sort(key=lambda x: x[0])

        return coeffs, ferr


    def _fit_point(self, rad, trial):

        yfit = 0.0
        for igfs in range(0, len(trial), 2):
            yfit += trial[igfs+1] * np.exp(-trial[igfs]*rad*rad)

        return yfit


    def _residue(self, trial):

        ngfs = len(trial) // 2
        rinc = (self.xraw[-1] - self.xraw[0])/(self.npts - 1)
        nsig = 1 + int(6.0*max(trial[::2])/np.sqrt(2.0)/rinc)

        yres = np.concatenate((
            np.array([self.yraw[ipts] - \
                self._fit_point(self.xraw[ipts], trial) \
                for ipts in range(self.npts)]),
            np.array([self._fit_point(self.rcut + isig*rinc, trial) \
                for isig in range(self.npts, max(nsig, self.npts))])))

        return np.linalg.norm(yres)


    def _residue_notail(self, trial):

        yres = np.array([self.yraw[ipts] - \
            self._fit_point(self.xraw[ipts], trial) \
            for ipts in range(self.npts)])

        return np.linalg.norm(yres)

