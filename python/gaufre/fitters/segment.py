#!/usr/bin/env python3

import os
import sys

import numpy as np
from scipy.optimize import lsq_linear, minimize


                    # ------------------------------------ #


class SegmentFitter(object):


    def __init__(self, ngfs, xraw, yraw):

        assert(ngfs > 2)
        assert(len(xraw) > 10)
        assert(len(xraw) == len(yraw))
        assert(xraw[-1] > np.finfo(np.float64).eps)
        assert(abs(yraw[-1]) < np.finfo(np.float64).eps)

        # Init problem dimensions
        # Note: rcut is not always the last point
        self.ngfs = ngfs
        self.ncut = len(xraw)
        self.rcut = xraw[-1]
        for ipts in range(len(xraw)-1, 1, -1):
            if ( abs(yraw[ipts]) > np.finfo(np.float64).eps ):
                break
            self.ncut = ipts
            self.rcut = xraw[ipts]
        self.rinc = (self.rcut - xraw[0])/(self.ncut - 1)
        assert(self.ncut > 0.5*len(xraw))

        # Import raw data
        self.xraw = np.concatenate((xraw, [xraw[-1] + (i+1)*self.rinc for i in range(self.ncut)]))
        self.yraw = np.concatenate((yraw, np.zeros(self.ncut)))
        self.npts = len(self.xraw)
        assert(len(self.xraw) == len(self.yraw))

        self.amin = 0.5*(3.0**2)/(self.rcut**2)
        self.amax = 0.5*((self.ncut-1)**2)/(9.0*(self.rcut**2))
        self.weights = np.ones(self.npts)


    def seek_fit(self, bounds=False, nsfs=3.0, nsrs=2.5, verbose=False):

        assert(nsfs > np.finfo(np.float64).eps)
        assert(nsrs > np.finfo(np.float64).eps)

        # Init
        self.amin = 0.5*(nsrs**2)/(self.rcut**2)
        self.amax = 0.5*((self.ncut-1)**2)/((nsfs*self.rcut)**2)
        ferr = 1000.0*max(abs(self.yraw))
        alpha = [0.1] + [float(i+1) for i in range(self.ngfs-1)]
        for igfs in range(self.ngfs):
            alpha, beta, tmp_err = self._find_one_exponent(alpha, igfs)

        # Look for a global minimum
        gerr = 1.0
        merr = 1.0
        maxiter = 100
        niter = 0
        while ( (gerr > 1.0e-6) and (merr > 1.0e-3) and (niter < maxiter) ):
            niter += 1
            alpha_old = np.array([alpha[igfs] for igfs in range(self.ngfs)])
            for igfs in range(self.ngfs):
                alpha, beta, tmp_err = self._find_one_exponent(alpha, igfs)

            gerr = max(abs(alpha - alpha_old))
            merr = max(abs(self._fit_func(alpha, beta) - self.yraw))
            if ( verbose ):
                print("N={iter}, error={err:.3e}, ||B||={norm}, a[1]={amin:.3e}, a[n]={amax:.3e}, gerr={grad:.3e}".format(
                        iter=niter, err=merr, norm=np.linalg.norm(beta),
                        amin=alpha[0], amax=alpha[-1], grad=gerr))

            werr = abs(self._fit_func(alpha, beta) - self.yraw)
            for ipts in range(self.npts):
                self.weights[ipts] += 0.5*werr[ipts]/merr
        ferr = max(werr)

        # Translate the final result
        trial = []
        for igfs in range(self.ngfs):
            trial.append([alpha[igfs], beta[igfs]])

        return trial, ferr


    def _find_global_min(self, func, i, a, b, lb, ub, mpts=100):

        assert(len(a) == len(b))
        assert(mpts > 1)

        if ( abs(ub - lb) < np.sqrt(np.finfo(np.float64).eps) ):
            return ub, func(i, ub, a, b)

        xf = np.linspace(lb, ub, mpts)
        for igm in range(2):
            prec = (ub - lb)/(mpts**(igm+1))
            yf = np.array([func(i, x, a, b) for x in xf])
            idx_min = np.argmin(yf)
            if ( idx_min == 0 ):
                xf = np.linspace(xf[idx_min], xf[idx_min] + prec, mpts)
            elif ( idx_min == mpts - 1 ):
                xf = np.linspace(xf[idx_min] - prec, xf[idx_min], mpts)
            else:
                xf = np.linspace(xf[idx_min] - prec, xf[idx_min] + prec, mpts)
        yf = np.array([func(i, x, a, b) for x in xf])
        idx_min = np.argmin(yf)

        return xf[idx_min], yf[idx_min]


    def _find_one_exponent(self, alpha, igfs, threshold=1.4):

        assert(threshold > np.finfo(np.float64).eps)

        beta = self._quad_min(alpha)
        if ( igfs == 0 ):
            x, merr2 = self._find_global_min(self._res_of_alpha, igfs,
                alpha, beta, self.amin, alpha[igfs+1]/threshold)
        elif ( igfs == self.ngfs - 1 ):
            x, merr2 = self._find_global_min(self._res_of_alpha, igfs,
                alpha, beta,
                alpha[igfs-1]*threshold, min(alpha[igfs] + 1.0, self.amax))
        else:
            x, merr2 = self._find_global_min(self._res_of_alpha, igfs,
                alpha, beta,
                alpha[igfs-1]*threshold, alpha[igfs+1]/threshold)

        alpha_new = sorted(alpha[:igfs]) + [x] + sorted(alpha[igfs+1:])
        beta_new = self._quad_min(alpha_new)

        return alpha_new, beta_new, merr2


    def _fit_func(self, alpha, beta):

        return np.array([self._fit_point(self.xraw[ipts], alpha, beta) \
                for ipts in range(self.npts)])


    def _fit_point(self, rad, alpha, beta):

        yfit = 0.0
        for igfs in range(self.ngfs):
            yfit += beta[igfs] * np.exp(-alpha[igfs]*(rad**2))

        return yfit


    def _quad_min(self, alpha):

        amqm = np.zeros((self.npts, self.ngfs))
        yrqm = np.zeros(self.npts)
        for ipts in range(self.npts):
            yrqm[ipts] = self.weights[ipts]*self.yraw[ipts]
            for igfs in range(self.ngfs):
                amqm[ipts][igfs] = self.weights[ipts] * \
                        np.exp(-alpha[igfs]*(self.xraw[ipts]**2))

        return np.linalg.lstsq(amqm, yrqm, rcond=-1)[0]


    def _residue(self, alpha, beta):

        yres = np.array([self.weights[ipts]*abs(self.yraw[ipts] - \
            self._fit_point(self.xraw[ipts], alpha, beta)) \
            for ipts in range(self.npts)])

        return np.linalg.norm(yres)**2


    def _res_of_alpha(self, igfs, x, alpha, beta):

        atmp = alpha[:igfs] + [x] + alpha[igfs+1:]
        btmp = self._quad_min(atmp)
        yres = np.array([self.weights[ipts]*abs(self.yraw[ipts] - \
            self._fit_point(self.xraw[ipts], atmp, btmp)) \
            for ipts in range(self.npts)])

        return np.linalg.norm(yres)**2

