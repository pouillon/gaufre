#!/usr/bin/python3

from lxml import etree
import yaml
try:
    from yaml import CLoader as MyLoader
except ImportError:
    from yaml import Loader as MyLoader

from numpy import float64

from gaufre.orbitals.siesta import SiestaOrbital


                    # ------------------------------------ #


class FdfNao2gtoReader(object):
    """Extracts a NAO2GTO block from a FDF file"""


    def __init__(self, fdf_path):


        # Define internal fields
        self.trials = []

        # Import FDF data
        fdf_data = []
        with open(fdf_path, "r") as fdf_file:
            fdf_data = fdf_file.read().split("\n")

        # Find the boundaries of the NAO2GTO block
        ifdf_beg = -1
        ifdf_end = -1
        for ifdf in range(len(fdf_data)):
            line = " ".join(fdf_data[ifdf].split())
            if ( line == "%block NAO2GTO" ):
                ifdf_beg = ifdf
            elif ( line == "%endblock NAO2GTO" ):
                ifdf_end = ifdf
        if ( (ifdf_beg == -1) and (ifdf_end == -1) ):
            raise IOError("NAO2GTO block not found")
        elif ( (ifdf_beg == -1) or (ifdf_end == -1) or (ifdf_beg > ifdf_end) ):
            raise IOError("Malformed NAO2GTO block")

        # Extract the Gaussian fitting trial vectors
        fdf_block = fdf_data[ifdf_beg+1:ifdf_end]
        while ( len(fdf_block) > 0 ):
            species, norbs = fdf_block.pop(0).split()[0:2]
            norbs = int(norbs)
            for iorb in range(norbs):
                qn_n, qn_l, zeta, ngfs = map(int, fdf_block.pop(0).split())
                gfit = []
                for igfs in range(ngfs):
                    alpha, beta = map(float64, fdf_block.pop(0).split())
                    gfit.append([alpha, beta])
                self.trials.append({
                    "species": species,
                    "qn_n": qn_n,
                    "qn_l": qn_l,
                    "zeta": zeta,
                    "trial": gfit,
                })


    def get_trial(self, species, qn_n, qn_l, zeta):

        trials = [item["trial"] for item in self.trials \
                     if ( (item["species"] == species) and \
                          (item["qn_n"] == qn_n) and \
                          (item["qn_l"] == qn_l) and \
                          (item["zeta"] == zeta) )]
        if ( len(trials) == 0 ):
            raise KeyError("No trial vector found for {:s}(n={:d}, l={:d}, z={:d})".format(
                species, qn_n, qn_l, zeta))
        elif ( len(trials) > 1 ):
            raise KeyError("More than one trial vector found for {:s}(n={:d}, l={:d}, z={:d})".format(
                species, qn_n, qn_l, zeta))

        return trials[0]


                    # ------------------------------------ #


class OrbitalReader(object):


    def __init__(self):

        # Elements to extract from the SIESTA ION file
        self.orbitals = []
        self._current = 0


    def __getitem__(self, index):

        return self.orbitals[index]


    def __iter__(self):

        return self


    def __len__(self):

        return len(self.orbitals)


    def __next__(self):

        if ( self._current >= len(self.orbitals) ):
            raise StopIteration
        self._current += 1

        return self.orbitals[self._current-1]


    def __reversed__(self):

        return reversed(self.orbitals)


                    # ------------------------------------ #


class IonXmlReader(OrbitalReader):


    def __init__(self, ion_path):

        # Elements to extract from the SIESTA ION file
        self.orbitals = []
        self._current = 0

        # Parse the XML file
        with open(ion_path, "rb") as ion_file:
            tree = etree.parse(ion_file)

        # Process XML contents
        root = tree.getroot()
        if ( root.tag == "ion" ):
            symbol = root.find("symbol").text.strip()

            for orb in root.find("paos"):

                # Extract and reformat orbital metadata
                orb_meta = {
                    "species": symbol,
                    "qn_n": int(orb.attrib["n"]),
                    "qn_l": int(orb.attrib["l"]),
                    "zeta": int(orb.attrib["z"]),
                    "polarized": bool(int(orb.attrib["ispol"])),
                    "population": float(orb.attrib["population"]),
                }

                # Extract and reformat orbital data points
                orb_data = []
                raw_data = orb.find("radfunc").find("data")
                for line in raw_data.text.strip().split("\n"):
                    orb_data.append([float(item) for item in line.split()])

                # Extract and reformat available NAO2GTO coefficients
                orb_trial = []
                nao2gto = orb.find("nao2gto")
                if ( nao2gto ):
                    for line in nao2gto.text.strip().split("\n"):
                        orb_trial.append([float(item) for item in line.split()])

                self.orbitals.append(SiestaOrbital(
                    points=orb_data,
                    trial=orb_trial,
                    **orb_meta))

        # Sort orbitals
        self.orbitals = sorted(self.orbitals,
            key=lambda x: (x.species, x.qn_n, x.qn_l, x.zeta))


                    # ------------------------------------ #


class RawOrbReader(OrbitalReader):


    def __init__(self, orb_paths):

        # Elements to extract from the SIESTA ORB files
        self.orbitals = []
        self._current = 0

        # Extract orbital data from each raw text file
        for orb_path in orb_paths:
            orb_data = []
            with open(orb_path, "r") as orb_file:
                orb_data = orb_file.read().strip().split("\n")

            species, qn_l, qn_n, zeta, polarized, population = \
                orb_data[0][1:].split()
            points = []
            for line in orb_data[2:]:
                points.append([float(item) for item in line.split() \
                    if len(line.split()) == 2])

            self.orbitals.append(SiestaOrbital(species.strip(),
                int(qn_n), int(qn_l), int(zeta), bool(int(polarized)),
                float(population), points))

        # Sort orbitals
        self.orbitals = sorted(self.orbitals,
            key=lambda x: (x.species, x.qn_n, x.qn_l, x.zeta))


                    # ------------------------------------ #


class YamlReader(OrbitalReader):


    def __init__(self, yml_path):

        # Elements to extract from the YAML file
        self.orbitals = []
        self._current = 0

        # Get documents from the YAML file
        orb_data = []
        with open(yml_path, "r") as yml_file:
            yml_data = yaml.load_all(yml_file, Loader=MyLoader)
            for item in yml_data:
                try:
                    orb_data.append(item["orbital"])
                except KeyError:
                    pass

        # Translate orbital data
        for orb in orb_data:
            orb_params = {}
            for key in ["species", "qn_n", "qn_l", "zeta",
                "population", "trial"]:
                orb_params[key] = orb[key]
            orb_params["points"] = orb["raw_data"]
            orb_params["polarized"] = False
            if ( orb["polarized"] in ("yes", True) ):
                orb_params["polarized"] = True
            self.orbitals.append(SiestaOrbital(**orb_params))

        # Sort orbitals
        self.orbitals = sorted(self.orbitals,
            key=lambda x: (x.species, x.qn_n, x.qn_l, x.zeta))

