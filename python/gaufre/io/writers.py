#!/usr/bin/python3

import os

from collections import OrderedDict
from lxml import etree
from yaml import load as yaml_load, dump as yaml_dump
try:
    from yaml import CLoader as YamlLoader, CDumper as YamlDumper
except ImportError:
    from yaml import Loader as YamlLoader, Dumper as YamlDumper

import numpy

from gaufre.orbitals.siesta import SiestaOrbital


                    # ------------------------------------ #


class FdfNao2gtoWriter(object):
    """Writes a NAO2GTO block to a FDF file"""


    def __init__(self, siesta_orbs):

        # Define internal fields
        self.orbitals = sorted(siesta_orbs,
            key=lambda x: (x.species, x.qn_n, x.qn_l, x.zeta))
        self.fdf_data = ""

        # Translate the list of orbitals into a FDF block
        old_species = ""
        self.fdf_data += "%block NAO2GTO\n"
        for orb in self.orbitals:
            if ( orb.species != old_species ):
                orb_count = len([x.species for x in self.orbitals \
                    if x.species == orb.species])
                self.fdf_data += "{s} {norb}\n".format(s=orb.species,
                    norb=orb_count)
                old_species = orb.species
            self.fdf_data += "{n} {l} {z} {ngfs}\n".format(n=orb.qn_n,
                l=orb.qn_l, z=orb.zeta, ngfs=len(orb.trial))
            for x, y in orb.trial:
                self.fdf_data += "{:24.8e} {:24.8e}\n".format(x, y)
        self.fdf_data += "%endblock NAO2GTO\n"


    def print_fdf(self, fdf_header=None):

        if ( fdf_header ):
            print(fdf_header, end="")
        print(self.fdf_data, end="")


    def write_fdf(self, fdf_path, fdf_header=None, overwrite=False):

        # Files will be overwritten only if the user explicitly says so
        if ( (not overwrite) and os.path.exists(fdf_path) ):
            raise IOError("File already exists:\n    {}".format(fdf_path))

        # We just write down the precomputed FDF block, since we don't want
        # to modify the orbital data
        with open(fdf_path, "w") as fdf_file:
            if ( fdf_header ):
                fdf_file.write(fdf_header)
            fdf_file.write(self.fdf_data)


                    # ------------------------------------ #


class RawOrbWriter(object):
    """Writes a series of SIESTA ORB files"""


    def __init__(self, siesta_orbs):

        # Elements to write to the SIESTA ORB files
        self.orbitals = sorted(siesta_orbs,
            key=lambda x: (x.species, x.qn_l, x.qn_n, x.zeta))


    def write_orbs(self, orb_destdir, overwrite=False):

        # Make sure we can write to the destination directory
        if ( not os.path.isdir(orb_destdir) ):
            raise IOError("Not a directory:\n    {}".format(orb_destdir))

        # Generate one file per orbital
        orb_index = 0
        old_qn = (0, 0)
        for orb in self.orbitals:

            # SIESTA indexes orbitals using the l and n quantum numbers, then
            # the zeta index, then the species
            new_qn = (orb.qn_l, orb.qn_n)
            if ( new_qn != old_qn ):
                orb_index += 1
                old_qn = new_qn
            orb_path = os.path.join(orb_destdir, 
                "ORB.S{index}.{zeta}.{species}".format(index=orb_index,
                    zeta=orb.zeta, species=orb.species))

            # Files will be overwritten only if the user explicitly says so
            if ( (not overwrite) and os.path.exists(orb_path) ):
                raise IOError("File already exists:\n    {}".format(orb_path))

            # Write header and raw data
            with open(orb_path, "w") as orb_file:
                orb_pol = 0
                if ( orb.polarized ):
                    orb_pol = 1
                orb_file.write(
                    "# {species} {l} {n} {z} {pol} {pop:%.4lf}\n".format(
                        species=orb.species, l=orb.qn_l, n=orb.qn_n,
                        z=orb.zeta, pol=orb_pol, pop=orb.population))
                orb_file.write("#(species label, l, n, z, is_polarized, popul)\n")
                for ipts in range(len(orb.xraw)):
                    orb_file.write("{:%.16le} {:%.16le}\n".format(
                        orb.xraw[ipts], orb.yraw[ipts]))


                    # ------------------------------------ #


class YamlWriter(object):
    """Writes a YAML file compatible with GAUFRE"""


    def __init__(self, siesta_orbs):

        # Elements to write to the SIESTA ORB files
        self.orbitals = sorted(siesta_orbs,
            key=lambda x: (x.species, x.qn_l, x.qn_n, x.zeta))

        # Build the data structures
        self.yml_docs = []
        for orb in self.orbitals:
            yml_data = OrderedDict()
            yml_data["ngfs"] = len(orb.trial)
            yml_data["npts"] = len(orb.xraw)
            yml_data["raw_data"] = zip(orb.xraw, orb.yraw)
            if ( len(orb.trial) > 0 ):
                yml_data["trial"] = trial
            else:
                yml_data["trial"] = "!!null"
            yml_data["residue"] = self.get_residue(orb)
            yml_data["is_valid"] = self.is_valid_data(orb)
            for key in ["species", "qn_n", "qn_l", "zeta", "polarized",
                        "population"]:
                yml_data[key] = get_attr(orb, key)
            yml_data["orbital_is_valid"] = self.is_valid_orbital(orb)

            self.yml_docs.append(yml_data)


    def get_residue(self, orb):

        npts = len(orb.xraw)
        yerr = numpy.array(npts)
        for ipts in range(npts):
            yerr = yraw[ipts]
            if ( len(orb.trial) > 0 ):
                for igfs in range(len(orb.trial)):
                    yerr -= orb.trial[igfs][1] \
                          * numpy.exp(-orb.trial[igfs][0] \
                          * orb.xraw[ipts] * orb.xraw[ipts])

        return numpy.sqrt(sum(yerr*yerr))


    def is_valid_data(self, orb):

        ngfs = len(orb.trial) // 2;
        npts = len(orb.xraw)

        trial_ok = False
        if ( ngfs > 0 ):
            trial_wrong = [item for item in orb.trial \
                if item[0] < numpy.float64.eps]
            if ( len(trial_wrong) == 0 ):
                trial_ok = True

        return ( trial_ok and (len(orb.yraw) == npts) and (npts > 2*ngfs) )


    def is_valid_orbital(self, orb):

        physics_ok = (
            (len(orb.species) in (1, 2, 3)) and (orb.qn_n >= 1) and
            (orb.qn_l > 0) and (orb.qn_l < orb.qn_n) and (orb.zeta >= 1) and
            (orb.population >= 0.0) )

        return ( self.is_valid_data(orb) and physics_ok )


    def write_yaml(self, yml_path, overwrite=False):

        # Files will be overwritten only if the user explicitly says so
        if ( (not overwrite) and os.path.exists(yml_path) ):
            raise IOError("File already exists:\n    {}".format(fdf_path))

        with open(yml_path, "w") as yml_file:
            yaml_dump_all(self.yml_docs, yml_file, explicit_start=True,
                explicit_end=True, version="1.2")

