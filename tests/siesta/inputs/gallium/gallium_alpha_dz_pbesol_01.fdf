SystemName      Ga, alfa phase
#               Coordinates taken from http://rruff.geo.arizona.edu/AMS/result.php
#               GGA, PBEsol
#               MeshCutOff energy of 400 Ry
#               10 x 10 x 10 Monkhorst-Pack mesh
SystemLabel     gallium_alpha_dzp_pbe_01

NumberOfAtoms        4
NumberOfSpecies      1
%block ChemicalSpeciesLabel
  1    31   Ga
%endblock ChemicalSpeciesLabel

LatticeConstant      1.000 Ang
%block LatticeVectors
  2.25535  0.00000 -3.82240
  0.00000  4.51670  0.00000
  2.25535  0.00000  3.82240
%endblock LatticeVectors

AtomicCoordinatesFormat   NotScaledCartesianAng
%block AtomicCoordinatesAndAtomicSpecies
 2.25535    1.90379    1.16583    1
 2.25535    4.16214    2.65656    1
 2.25535    0.35456    4.98823    1
 2.25535    2.61291    6.47897    1
%endblock AtomicCoordinatesAndAtomicSpecies

%block kgrid_Monkhorst_Pack
 10   0  0   0.0
  0  10  0   0.0
  0  0  10   0.0
%endblock kgrid_Monkhorst_Pack

XC.functional           GGA
XC.authors              PBEsol
SpinPolarized           .false.

MeshCutoff              400.0 Ry
MaxSCFIterations        200
DM.MixingWeight         0.00100
DM.NumberPulay          5
DM.Tolerance            1.d-4
ElectronicTemperature   0.02 Ry 
SCF.MixAfterConvergence .false.
Diag.DivideAndConquer   .false.

#
# Options for saving/reading information
#

DM.UseSaveDM            .true.       # Use DM Continuation files
MD.UseSaveXV            .false.      # Use stored positions and velocities
MD.UseSaveCG            .false.      # Use CG history information

WriteForces             .true.
WriteCoorStep           .true.
WriteIonPlotFiles       .true.
WriteMDHistory          .true.

#
# Molecular dynamics and relaxations
#

MD.TypeOfRun            cg          # Type of dynamics:
                                    #   - CG
                                    #   - Verlet
                                    #   - Nose
                                    #   - Parrinello-Rahman
                                    #   - Nose-Parrinello-Rahman
                                    #   - Anneal
                                    #   - FC
MD.VariableCell         .true.      # The lattice is relaxed together with
                                    #   the atomic coordinates?
MD.NumCGsteps           10          # Number of CG steps for
                                    #   coordinate optimization
MD.MaxCGDispl           0.3 Bohr    # Maximum atomic displacement
                                    #   in one CG step
MD.MaxForceTol          0.01 eV/Ang # Tolerance in the maximum
                                    #   atomic force
MD.MaxStressTol         0.0001 eV/Ang**3


##
## Optical constants block
##
#
#OpticalCalculation       .true.
#Optical.Broaden          0.1 eV
#Opical.Energy.Minimum    0 eV
#Optical.Energy.Maximum   20 eV
#
#%block Optical.Mesh
# 90 90 90
#%endblock Optical.Mesh
#
#BandLinesScale          ReciprocalLatticeVectors
#%block BandLines
#1    0.000000   0.5000   0.000000   X    # Begin at X
#30   0.000000   0.0000   0.000000   G    # 30 points from X to Gamma
#30   0.336933   0.0000   0.336933   Y    # 30 points from Gamma to Y
#30   0.663067   0.0000  -0.336933   Y1   # 30 points from Y to Y1
#30   0.500000   0.0000  -0.500000   Z    # 30 points from Y1 to Zeta
#30   0.000000   0.0000   0.000000   G    # 30 points from Zeta to Gamma
#%endblock BandLines
#
#%block ProjectedDensityOfStates
#  -150.00  5.00  0.800 3000  eV
#%endblock ProjectedDensityOfStates
#
#%PDOS.kgrid_Monkhorst_Pack
#   60  0  0  0.5
#    0 60  0  0.5
#    0  0 60  0.5
#%end PDOS.kgrid_Monkhorst_Pack

%block PS.lmax
   Ga    3
%endblock PS.lmax

%Block PAO.Basis
Ga   5      0.06781
 n=3   0   1   E    36.64088     0.46599
     3.04718
     1.00000
 n=4   0   3   E   148.52316     1.36388
     7.45068     4.50486     3.01314
     1.00000     1.00000     1.00000
 n=3   1   1   E    15.08547     1.55303
     4.10718
     1.00000
 n=4   1   2   E     1.49215     3.41071
     7.99310     5.36660
     1.00000     1.00000
 n=3   2   2   E    97.42809     4.21854
     5.94251     3.02559
     1.00000     1.00000
%EndBlock PAO.Basis
