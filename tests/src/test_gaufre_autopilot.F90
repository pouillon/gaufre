! *** Program: test_gaufre ***

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

! *****************************************************************************
!> \brief Tests GAUFRE fitting using the implementation specified on the
!!        command line
!!
!! \author Yann Pouillon
!!
!! \par History
!!      - 04.2018 Created [Yann Pouillon]
! *****************************************************************************
program test_gaufre

  use, intrinsic :: iso_fortran_env, only: error_unit, output_unit

  use gaufre

  implicit none

  ! Constants
  integer, parameter :: PATH_MAXLEN = 1024   !< Maximum file path length

  ! Scalars
  character(len=GAUFRE_ERRMSG_LEN) :: errmsg
  character(len=PATH_MAXLEN) :: method_str, prog_path, prog_name, &
&   test_pfx, test_out
  logical, allocatable :: run_ok(:)
  integer :: arglen, argstat, errno, iarg, istr, nargs, method, ngfs, npts
  type(gaufre_autopilot_t) :: fit_pilot
  type(gaufre_orbital_t) :: orb_data

  ! Arrays
  character(len=PATH_MAXLEN), pointer :: orb_paths(:) => null()
  real(gfdp) :: marks(3)

  ! ---------------------------------------------------------------------------

  ! Init error data
  errno = GAUFRE_ERR_OK
  errmsg = repeat(" ", GAUFRE_ERRMSG_LEN)

  ! Init scalar variables
  method = GAUFRE_FIT_UNKNOWN

  ! Get program name
  call get_command_argument(0, prog_path)
  istr = scan(prog_path, "/", back=.true.)
  write(prog_name, fmt='(A)') trim(prog_path(istr+1:))

  ! Get list of files to process from command line
  nargs = command_argument_count() - 2
  if ( nargs <= 0 ) then
    write(output_unit, fmt='("Usage: ",A," method prefix file [file ...]")') &
&     trim(prog_name)
    write(output_unit, fmt='(A)') &
&     "       where 'method' is the index of the implementation to use"
    write(output_unit, fmt='(A)') &
&     "       (see gaufre_common.F90 for details)"
    stop 1
  else
    call get_command_argument(number=1, value=method_str)
    read(method_str, fmt='(I2)', iostat=argstat) method
    if ( argstat /= 0 ) then
      method = GAUFRE_FIT_UNKNOWN
      write(unit=error_unit, fmt='(A,": Error: ",A)') &
&       trim(prog_name), "Could not get method index from command line"
      stop 1
    end if
    call get_command_argument(number=2, value=test_pfx)
    allocate(orb_paths(nargs))
    write(*,*) "ORB_PATHS: ", shape(orb_paths)
    do iarg = 3,nargs+2
      call get_command_argument(number=iarg, value=orb_paths(iarg-2), &
&       length=arglen, status=argstat)
      if ( argstat < 0 ) then
        write(unit=error_unit, fmt='("Warning: ",A,1X,I2,1X,A)') &
&       "argument", iarg, "has been truncated"
      end if
      if ( argstat > 0 ) then
        write(unit=error_unit, fmt='("Warning: ",A,1X,I2,1X,A)') &
&         "argument", iarg, "could not be retrieved"
      end if
      write(unit=output_unit, fmt='("ARGUMENT ",I2.2," = [",A,"]")') &
&       iarg, trim(orb_paths(iarg-2))
    end do
  end if

  ! Banner
  write(unit=output_unit, fmt='(A," - Version ",A/)') &
&   trim(prog_name), trim(PACKAGE_VERSION)
  write(unit=output_unit, fmt='("METHOD  : ",I2.2," (",A,")")') &
&   method, trim(gaufre_method_name(method))
  write(unit=output_unit, fmt='("NORBS   : ",I2.2)') nargs
  write(unit=output_unit, fmt='(A)') ""

  ! Check that the requested method is implemented
  if ( .not. gaufre_has_method(method) ) then
    if ( method /= GAUFRE_FIT_UNKNOWN ) then
      write(unit=error_unit, fmt='("Error: ",A,1X,I2,1X,A)') &
        "method", method, "is not available"
      stop 2
    end if
  end if

                    ! ------------------------------------ !

  ! Prepare YAML file to dump the data
  test_out = repeat(" ", PATH_MAXLEN)
  write(test_out, fmt='(A,".yml")') trim(test_pfx)
  open(unit=20, file=trim(test_out), status="replace", action="write", &
&   form="formatted", access="sequential")
  write(unit=20, fmt='("%YAML",1X,A)') "1.1"

  ! Display most important global constants
  write(unit=output_unit, fmt='(A," = ",E12.3)') &
&   "GAUFRE_EPS_MIN", GAUFRE_EPS_MIN
  write(unit=output_unit, fmt='(A," = ",E12.3)') &
&   "GAUFRE_EPS_OPT", GAUFRE_EPS_OPT
  write(unit=output_unit, fmt='(A," = ",E12.3)') &
&   "GAUFRE_EPS_MAX", GAUFRE_EPS_MAX
  write(unit=output_unit, fmt='(A)')

  ! Display table header
  write(unit=output_unit, fmt='(A)') &
&   "+----+----+----+----+----+----+----------+----------+----------+----------+"
  write(unit=output_unit, fmt='(6("|",A4),4("|",A10),"|")') &
&   "N", "L", "ZETA", "NGFS", "NPTS", "CONV", "RESIDUE", "M(AMAX)", &
&   "M(AMIN)", "M(RES)"
  write(unit=output_unit, fmt='(A)') &
&   "+----+----+----+----+----+----+----------+----------+----------+----------+"
  ! Prepare result monitor
  allocate(run_ok(nargs))
  run_ok(:) = .false.

  ! Process each file, one by one
  do iarg = 1,nargs

    ! Reset error status
    errno = GAUFRE_ERR_OK

    ! Load the orbital data
    if ( errno == GAUFRE_ERR_OK ) then
      call orb_data%from_siesta(trim(orb_paths(iarg)), gfstat=errno, &
&       gfmsg=errmsg)
      if ( errno /= GAUFRE_ERR_OK ) then
        write(unit=error_unit, fmt='("Error: ",A)') trim(errmsg)
      end if
    end if

    ! Configure the fit
    if ( errno == GAUFRE_ERR_OK ) then
      call fit_pilot%init(method=method, gfstat=errno, gfmsg=errmsg)
      if ( errno /= GAUFRE_ERR_OK ) then
        write(unit=error_unit, fmt='("Error: ",A)') trim(errmsg)
      end if
    end if

    ! Fit the data
    if ( errno == GAUFRE_ERR_OK ) then
      call fit_pilot%exec(orb_data, marks=marks, gfstat=errno, gfmsg=errmsg)
      if ( errno /= GAUFRE_ERR_OK ) then
        write(unit=error_unit, fmt='("Error: ",A)') trim(errmsg)
      end if
    end if

    ! Set result monitor
    if ( errno == GAUFRE_ERR_OK ) then
      run_ok(iarg) = .true.
    end if

    ! Report results
    ngfs = orb_data%get_ngfs()
    npts = orb_data%get_npts()
    write(unit=output_unit, fmt='(5("|",I4),"|",L4,4("|",E10.2),"|")') &
&     orb_data%qn_n, orb_data%qn_l, orb_data%zeta, &
&     ngfs, npts, run_ok(iarg), orb_data%get_residue(), marks(:)

    ! Dump the data
    write(unit=20, fmt='("---")')
    call orb_data%dump("orbital", dump_unit=20, dump_all=.true.)

    ! Clean-up the mess
    call fit_pilot%free()
    call orb_data%free()

  end do

  ! Display table footer
  write(unit=output_unit, fmt='(A)') &
&   "+----+----+----+----+----+----+----------+----------+----------+----------+"
  write(unit=output_unit, fmt='(A)')

  ! Close YAML file
  write(unit=20, fmt='("...")')
  close(unit=20)

  ! Exit with an adequate return value
  if ( .not. all(run_ok) ) stop 3

end program test_gaufre
